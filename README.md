# BPMN-EDITOR

## Installation

You should install composer, then run `composer install`

In the `.env` file, you should add your database credentials and mail dsn.

You should also go on the [pusher.com](pusher.com) website to create an application and register the parameters.

## Database

You can let symfony create your database using the command `./bin/console doctrine:database:create`.

Then you have to run the command `./bin/console doctrine:migrations:migrate` to run all the migrations.

## Update

Once you have checkout the new tag, you should run the following command :

 - Update the vendor : `composer install`
 - Update the database : `./bin/console doctrine:migrations:migrate`
 - Clear the cache : `./bin/console cache:clear`
