<?php

namespace App\Event;

use App\Entity\File;
use App\Entity\User;
use App\Facade\FileFacade;
use Symfony\Contracts\EventDispatcher\Event;

class FileEvent extends Event
{
    const UPDATE = 'file.update';
    const ADD_ATTENTION = 'file.add_attention';
    const DELETE_ATTENTION = 'file.delete_attention';

    public function __construct(
        protected ?File $file,
        protected ?User $user,
        protected ?array $coordinates = [],
        protected ?string $color = null,
        protected ?string $fileId = null
    ) {}

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function getFileId(): ?string
    {
        return $this->fileId;
    }

    public static function addAttention(User $user, array $coordinates, string $color, string $fileId)
    {
        return new self(null, $user, $coordinates, $color, $fileId);
    }

    public static function deleteAttention(User $user, string $fileId)
    {
        return new self(null, $user, null, null, $fileId);
    }

    public static function updateFile(File $file, User $user)
    {
        return new self($file, $user, null, null, $file->getUuid());
    }
}