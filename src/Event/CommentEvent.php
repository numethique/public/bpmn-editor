<?php

namespace App\Event;

use App\Entity\Comment;
use App\Facade\CommentFacade;
use Symfony\Contracts\EventDispatcher\Event;

class CommentEvent extends Event
{
    const ADD = 'comment.add';
    const UPDATE = 'comment.update';
    const DELETE = 'comment.delete';

    public function __construct(
        protected Comment $comment,
        protected ?CommentFacade $commentFacade = null,
        protected ?string $originAppInstanceId = null
    ) {}

    public function getComment(): Comment
    {
        return $this->comment;
    }

    public function getCommentFacade(): CommentFacade
    {
        return $this->commentFacade;
    }

    public function getOriginAppInstanceId(): ?string
    {
        return $this->originAppInstanceId;
    }

    public static function addComment(Comment $comment, CommentFacade $commentFacade): CommentEvent
    {
        return new self($comment, $commentFacade, $commentFacade->originAppInstanceId);
    }

    public static function updateComment(Comment $comment, CommentFacade $commentFacade): CommentEvent
    {
        return new self($comment, $commentFacade, $commentFacade->originAppInstanceId);
    }

    public static function deleteComment(Comment $comment, $originAppInstanceId): CommentEvent
    {
        return new self($comment, null, $originAppInstanceId);
    }
}