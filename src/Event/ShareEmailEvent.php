<?php

namespace App\Event;

use App\Entity\Share;
use Symfony\Contracts\EventDispatcher\Event;

class ShareEmailEvent extends Event
{
    const SEND_MAIL = 'share.email';

    public function __construct(
        protected Share $share,
        protected $email,
        protected $message
    ) {}

    public function getShare(): Share
    {
        return $this->share;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    public static function create(Share $share, string $email, string $message)
    {
        return new self($share, $email, $message);
    }
}
