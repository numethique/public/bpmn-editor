<?php

namespace App\Event;

use App\Entity\Invitation;
use Symfony\Contracts\EventDispatcher\Event;

class InvitationEvent extends Event
{
    const CREATE = 'invitation.create';

    public function __construct(
        protected Invitation $invitation
    ) {}

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public static function create(Invitation $invitation)
    {
        return new self($invitation);
    }
}