<?php

namespace App\Facade;

class ProjectFacade implements FacadeInterface
{
    public $id;
    public $name;
    public $slug;
    public $permissionAccess;
    public $collaborators = [];
    public $files = [];
    public $folders = [];
    public $filesCount;
    public $created;
    public $lastModified;
    public $type = "DEFAULT";
    public $organizationId;
    public $invitations = [];
    public $admins = [];
}
