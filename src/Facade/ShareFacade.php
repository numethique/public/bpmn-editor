<?php

namespace App\Facade;

class ShareFacade implements FacadeInterface
{
    public $id;
    public $fileId;
    public $file;
    public $password;
}