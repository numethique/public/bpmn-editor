<?php

namespace App\Facade;

class MoveFileFacade
{
    public $fileIds;
    public $targetFolderId;
    public $targetProjectId;
}