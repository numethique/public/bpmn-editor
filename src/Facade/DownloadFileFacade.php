<?php

namespace App\Facade;

class DownloadFileFacade implements FacadeInterface
{
    public $fileIds = [];
    public $folderIds = [];
}
