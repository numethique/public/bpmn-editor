<?php

namespace App\Facade;

class ProjectInvitationFacade implements FacadeInterface
{
    public $emails = [];
    public $text;
    public $projectAccess;
}