<?php

namespace App\Facade;

class ShareEmailFacade implements FacadeInterface
{
    public $emails = [];
    public $message;
}
