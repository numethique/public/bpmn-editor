<?php

namespace App\Facade;

class FileFacade implements FacadeInterface
{
    public $author;
    public $callActivityLinks;
    public $changed;
    public $content;
    public $created;
    public $folderId;
    public $id;
    public $name;
    public $organizationId;
    public $originAppInstanceId;
    public $passwordProtected;
    public $processId;
    public $projectId;
    public $relationId;
    public $revision;
    public $slug;
    public $fileSlug;
    public $type;
    public $shareId;
    public $children = [];
    public $parents = [];
}
