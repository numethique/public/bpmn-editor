<?php

namespace App\Facade;

class SelfFacade implements FacadeInterface
{
    public $email;
    public $id;
    public $name;
    public $username;
    public $verified = true;
    public $segment = "C";
    public $organizations = [];
}