<?php

namespace App\Facade;

class FolderFacade implements FacadeInterface
{
    public $id;
    public $name;
    public $slug;
    public $projectId;
    public $createdBy;
    public $children = [];
    public $parentId;
    public $files = [];
}