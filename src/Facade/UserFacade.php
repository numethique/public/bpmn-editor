<?php

namespace App\Facade;

class UserFacade implements FacadeInterface
{
    public $email;
    public $id;
    public $name;
    public $username;
}