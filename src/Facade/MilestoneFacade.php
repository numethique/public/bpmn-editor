<?php

namespace App\Facade;

class MilestoneFacade implements FacadeInterface
{
    public $id;
    public $fileId;
    public $name;
    public $authorId;
    public $authorName;
    public $created;
    public $content;
}