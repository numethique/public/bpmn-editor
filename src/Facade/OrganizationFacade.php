<?php

namespace App\Facade;

class OrganizationFacade implements FacadeInterface
{
    public $id;
    public $name;
    public $created;
    public $admins;
}