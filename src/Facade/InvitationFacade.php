<?php

namespace App\Facade;

class InvitationFacade implements FacadeInterface
{
    public $email;
    public $tokenId;
    public $status = 'SENT';
    public $permissionAccess;
}
