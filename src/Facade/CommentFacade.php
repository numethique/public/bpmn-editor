<?php

namespace App\Facade;

class CommentFacade implements FacadeInterface
{
    public $id;
    public $fileId;
    public $content;
    public $createdAt;
    public $lastChanged;
    public $author;
    public $reference;
    public $originAppInstanceId;
}