<?php

namespace App\Security;

use App\Entity\File;
use App\Entity\ProjectUser;
use App\Entity\User;
use App\Repository\ProjectUserRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FileVoter extends Voter
{
    const EDIT = 'edit';
    const COMMENT = 'comment';
    const VIEW = 'view';

    public function __construct(protected ProjectUserRepository $projectUserRepository)
    {}

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::EDIT, self::VIEW, self::COMMENT])) {
            return false;
        }

        return $subject instanceof File;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        /** @var File $file */
        $file = $subject;
        $projectUser = $this->projectUserRepository->findOneByUserIdAndInternalProjectId($user->getId(), $file->getProject()->getId());

        if (!$projectUser instanceof ProjectUser) {
            return false;
        }

        $permission = $projectUser->getPermission();

        if (
            ProjectUser::OWNER == $permission
            || ProjectUser::EDITOR == $permission
        ) {
            return true;
        }

        if (ProjectUser::COMMENT == $permission && self::COMMENT == $attribute) {
            return true;
        }

        return self::VIEW == $attribute;
    }
}
