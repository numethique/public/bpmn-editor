<?php

namespace App\Transformer;

use App\Facade\FacadeInterface;

interface TransformerInterface
{
    const CREATION = 'creation';
    const ONLY_FILE = 'only_file';
    const UPDATE = 'update';
    const SHARE = 'share';
    const DISPLAY_SHARE = 'display_share';
    const ADMIN = 'admin';

    public function transform($mixed, string|null $case = null): FacadeInterface;
    public function reverseTransform(FacadeInterface $facade, $mixed, string|null $case = null);
}
