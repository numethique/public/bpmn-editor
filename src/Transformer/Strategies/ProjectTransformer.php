<?php

namespace App\Transformer\Strategies;

use App\Entity\Project;
use App\Facade\FacadeInterface;
use App\Facade\ProjectFacade;
use App\Repository\OrganizationRepository;
use App\Repository\ProjectUserRepository;
use App\Transformer\TransformerInterface;
use Cocur\Slugify\Slugify;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProjectTransformer implements TransformerInterface
{
    public function __construct(
        protected ProjectUserTransformer $projectUserTransformer,
        protected FolderTransformer      $folderTransformer,
        protected FileTransformer        $fileTransformer,
        protected OrganizationRepository $organizationRepository,
        protected InvitationTransformer  $invitationTransformer,
        protected TokenStorageInterface  $tokenStorage,
        protected ProjectUserRepository  $projectUserRepository
    ) {}
    /**
     * @param Project $mixed
     * @param string|null $case
     *
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new ProjectFacade();

        $slugify = new Slugify();

        $facade->id = $mixed->getUuid();
        $facade->name = $mixed->getName();

        $projectUser = $this->projectUserRepository->findOneByEmailAndProjectId(
            $this->tokenStorage->getToken()->getUser()->getEmail(),
            $mixed->getUuid()
        );
        $facade->permissionAccess = $projectUser->getPermission();

        if (self::ADMIN == $case) {
            foreach ($mixed->getProjectCollaborators() as $collaborator) {
                $facade->admins[] = $this->projectUserTransformer->transform($collaborator);
                break;
            }
        } else {
            $facade->slug = $mixed->getUuid() . "--" . $slugify->slugify($mixed->getName());
            foreach ($mixed->getProjectCollaborators() as $collaborator) {
                $facade->collaborators[] = $this->projectUserTransformer->transform($collaborator);
            }
            foreach ($mixed->getFolders() as $folder) {
                if (null === $folder->getParent()) {
                    $facade->folders[] = $this->folderTransformer->transform($folder);
                }
            }
            foreach ($mixed->getFiles() as $file) {
                if (null === $file->getFolder()) {
                    $facade->files[] = $this->fileTransformer->transform($file);
                }
            }
            foreach ($mixed->getInvitations() as $invitation) {
                $facade->invitations[] = $this->invitationTransformer->transform($invitation);
            }
            $facade->filesCount = $mixed->getFiles()->count();
            $facade->created = (int) $mixed->getCreatedAt()->format('Uv');
            $facade->lastModified = (int) $mixed->getUpdatedAt()->format('Uv');
            $facade->organizationId = $mixed->getOrganization()->getUuid();
        }

        return $facade;
    }

    /**
     * @param ProjectFacade $facade
     * @param Project $mixed
     * @param string|null $case
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        $mixed->setName($facade->name);
        $mixed->setUpdatedAt(new \DateTimeImmutable());

        if (null !== $facade->organizationId) {
            $mixed->setOrganization($this->organizationRepository->findOneByUuid($facade->organizationId));
        }

        return $mixed;
    }
}