<?php

namespace App\Transformer\Strategies;

use App\Entity\ProjectUser;
use App\Facade\ProjectUserFacade;
use App\Facade\FacadeInterface;
use App\Transformer\TransformerInterface;

class ProjectUserTransformer implements TransformerInterface
{
    /**
     * @param ProjectUser $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, string|null $case = null): FacadeInterface
    {
        $facade = new ProjectUserFacade();

        $facade->id = $mixed->getUser()->getUuid();
        $facade->name = $mixed->getUser()->getName();
        $facade->username = $mixed->getUser()->getName();
        $facade->email = $mixed->getUser()->getEmail();
        $facade->permissionAccess = $mixed->getPermission();

        return $facade;
    }

    public function reverseTransform(FacadeInterface $facade, $mixed, string|null $case = null)
    {
    }
}
