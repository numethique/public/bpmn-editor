<?php

namespace App\Transformer\Strategies;

use App\Entity\Folder;
use App\Facade\FacadeInterface;
use App\Facade\FolderFacade;
use App\Repository\FolderRepository;
use App\Repository\ProjectRepository;
use App\Transformer\TransformerInterface;
use Cocur\Slugify\Slugify;

class FolderTransformer implements TransformerInterface
{
    public function __construct(
        protected FileTransformer $fileTransformer,
        protected FolderRepository $folderRepository,
        protected ProjectRepository $projectRepository,
        protected UserTransformer $userTransformer
    ) {}

    /**
     * @param Folder $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, string|null $case = null): FacadeInterface
    {
        $facade = new FolderFacade();

        $slugify = new Slugify();

        $facade->id = $mixed->getUuid();
        $facade->name = $mixed->getName();
        $facade->slug = $mixed->getUuid() . "--" . $slugify->slugify($mixed->getName());
        $facade->projectId = $mixed->getProject()->getUuid();
        $facade->createdBy = $this->userTransformer->transform($mixed->getCreatedBy());
        if ($mixed->getParent() instanceof Folder) {
            $facade->parentId = $mixed->getParent()->getUuid();
        }
        foreach ($mixed->getChildrens() as $children) {
            $facade->children[] = $this->transform($children, $case);
        }
        foreach ($mixed->getFiles() as $file) {
            $facade->files[] = $this->fileTransformer->transform($file, $case = null);
        }

        return $facade;
    }

    /**
     * @param FolderFacade $facade
     * @param Folder $mixed
     * @param string|null $case
     *
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, string|null $case = null)
    {
        $mixed->setUpdatedAt(new \DateTimeImmutable());

        if (null !== $facade->name) {
            $mixed->setName($facade->name);
        }

        if (null !== $facade->parentId) {
            $mixed->setParent($this->folderRepository->findOneByUuid($facade->parentId));
        }

        if (null !== $facade->projectId) {
            $mixed->setProject($this->projectRepository->findOneByUuid($facade->projectId));
        }

        return $mixed;
    }

}