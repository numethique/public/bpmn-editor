<?php

namespace App\Transformer\Strategies;

use App\Entity\Organization;
use App\Facade\FacadeInterface;
use App\Facade\OrganizationFacade;
use App\Repository\UserOrganizationRepository;
use App\Transformer\TransformerInterface;

class OrganizationTransformer implements TransformerInterface
{
    public function __construct(
        protected UserTransformer $authorTransformer,
        protected UserOrganizationRepository $userOrganizationRepository
    ) {}

    /**
     * @param Organization $mixed
     * @param string|null $case
     *
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new OrganizationFacade();

        $facade->id = $mixed->getUuid();
        $facade->name = $mixed->getName();
        $facade->created = $mixed->getCreatedAt()->getTimestamp();
        $admins = $this->userOrganizationRepository->findByOrganizationIdWithAdminRights($mixed->getUuid());
        foreach ($admins as $admin) {
            $facade->admins[] = $this->authorTransformer->transform($admin->getUser());
        }

        return $facade;
    }

    /**
     * @param OrganizationFacade $facade
     * @param Organization $mixed
     * @param string|null $case
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        if ($facade->name !== null) {
            $mixed->setName($facade->name);
        }
    }
}
