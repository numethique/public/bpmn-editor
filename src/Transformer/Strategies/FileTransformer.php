<?php

namespace App\Transformer\Strategies;

use App\Entity\File;
use App\Entity\Folder;
use App\Entity\Organization;
use App\Entity\Project;
use App\Entity\Share;
use App\Facade\FacadeInterface;
use App\Facade\FileFacade;
use App\Repository\FolderRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProjectRepository;
use App\Transformer\TransformerInterface;
use Cocur\Slugify\Slugify;

class FileTransformer implements TransformerInterface
{

    public function __construct(
        protected UserTransformer        $authorTransformer,
        protected FolderRepository       $folderRepository,
        protected ProjectRepository      $projectRepository,
        protected OrganizationRepository $organizationRepository
    ) {}

    /**
     * @param File $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, string|null $case = null): FacadeInterface
    {
        $facade = new FileFacade();

        $slugify = new Slugify();

        $facade->name = $mixed->getName();
        $facade->type = $mixed->getType();

        if (self::DISPLAY_SHARE == $case) {
            $facade->content = $mixed->getContent();
        } else {
            $facade->slug = $mixed->getUuid() . "--" . $slugify->slugify($mixed->getName());
            $facade->fileSlug = $mixed->getUuid() . "--" . $slugify->slugify($mixed->getName());

            if (self::SHARE !== $case) {
                $facade->id = $mixed->getUuid();
                $facade->created = (int) $mixed->getCreatedAt()->format('Uv');
                $facade->changed = (int) $mixed->getUpdatedAt()->format('Uv');
                $facade->projectId = $mixed->getProject()->getUuid();
                if ($mixed->getFolder() instanceof Folder) {
                    $facade->folderId = $mixed->getFolder()->getUuid();
                }
                if ($mixed->getOrganization() instanceof Organization) {
                    $facade->organizationId = $mixed->getOrganization()->getUuid();
                }
                $facade->revision = $mixed->getRevision();
                $facade->relationId = $mixed->getRelationId();
                $facade->processId = "Process_4a12a51f-7150-4b25-af64-a9b9dc1ce9bc";
                $facade->author = $this->authorTransformer->transform($mixed->getAuthor(), $case);
                if ($mixed->getShare() instanceof Share) {
                    $facade->shareId = $mixed->getShare()->getUuid();
                    $facade->passwordProtected = $mixed->getShare()->isPasswordProtected();
                }

                if (self::ONLY_FILE === $case) {
                    $facade->content = $mixed->getContent();
                }
            }
        }

        return $facade;
    }

    /**
     * @param FileFacade $facade
     * @param File $mixed
     * @param string|null $case
     * @return mixed
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, string|null $case = null)
    {
        $mixed->setUpdatedAt(new \DateTimeImmutable());
        $mixed->setRevision($mixed->getRevision() + 1);

        if (null !== $facade->name) {
            $mixed->setName($facade->name);
        }

        if (self::CREATION == $case && null !== $facade->type) {
            $mixed->setType($facade->type);
        }

        if (null !== $facade->folderId) {
            $mixed->setFolder($this->folderRepository->findOneByUuid($facade->folderId));
        }

        if (null !== $facade->content) {
            $mixed->setContent($facade->content);
        }

        if (null !== $facade->relationId) {
            $mixed->setRelationId($facade->relationId);
        }

        if (null !== $facade->projectId) {
            $mixed->setProject($this->projectRepository->findOneByUuid($facade->projectId));
        }

        if (null !== $facade->organizationId) {
            $mixed->setOrganization($this->organizationRepository->findOneByUuid($facade->organizationId));
        }

        if ($mixed->getProject() instanceof Project) {
            $mixed->getProject()->setUpdatedAt(new \DateTimeImmutable());
        }

        if (null !== $facade->originAppInstanceId) {
            $mixed->setOriginAppInstanceId($facade->originAppInstanceId);
        }

        return $mixed;
    }
}
