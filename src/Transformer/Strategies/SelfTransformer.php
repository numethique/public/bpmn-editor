<?php

namespace App\Transformer\Strategies;


use App\Entity\User;
use App\Facade\FacadeInterface;
use App\Facade\SelfFacade;
use App\Transformer\TransformerInterface;

class SelfTransformer implements TransformerInterface
{
    public function __construct(
        protected OrganizationTransformer $organizationTransformer
    )
    {}

    /**
     * @param User $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new SelfFacade();

        $facade->id = $mixed->getUuid();
        $facade->email = $mixed->getEmail();
        $facade->name = $mixed->getName();
        $facade->username = $mixed->getName();

        foreach ($mixed->getOrganizations() as $organization) {
            $facade->organizations[] = $this->organizationTransformer->transform($organization);
        }

        return $facade;
    }

    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        // TODO: Implement reverseTransform() method.
    }

}