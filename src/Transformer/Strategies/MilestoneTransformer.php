<?php

namespace App\Transformer\Strategies;

use App\Entity\Milestone;
use App\Facade\FacadeInterface;
use App\Facade\MilestoneFacade;
use App\Repository\FileRepository;
use App\Transformer\TransformerInterface;

class MilestoneTransformer implements TransformerInterface
{
    public function __construct(
        protected FileRepository $fileRepository
    ) {}

    /**
     * @param Milestone $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new MilestoneFacade();

        $facade->id = $mixed->getUuid();
        $facade->name = $mixed->getName();
        $facade->authorName = $mixed->getAuthor()->getName();
        $facade->authorId = $mixed->getAuthor()->getUuid();
        $facade->created = (int) $mixed->getCreatedAt()->format('Uv');
        $facade->fileId = $mixed->getFile()->getUuid();
        $facade->content = $mixed->getContent();

        return $facade;
    }

    /**
     * @param MilestoneFacade $facade
     * @param Milestone $mixed
     * @param string|null $case
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        $mixed->setName($facade->name);

        if ($case !== self::UPDATE) {
            $file = $this->fileRepository->findOneByUuid($facade->fileId);
            $mixed->setContent($file->getContent());
            $mixed->setFile($file);
        }
    }
}