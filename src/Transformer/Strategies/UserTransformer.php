<?php

namespace App\Transformer\Strategies;

use App\Entity\User;
use App\Facade\UserFacade;
use App\Facade\FacadeInterface;
use App\Transformer\TransformerInterface;

class UserTransformer implements TransformerInterface
{
    /**
     * @param User $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, string|null $case = null): FacadeInterface
    {
        $facade = new UserFacade();

        $facade->id = $mixed->getUuid();
        $facade->name = $mixed->getName();
        $facade->username = $mixed->getName();
        $facade->email = $mixed->getEmail();

        return $facade;
    }

    public function reverseTransform(FacadeInterface $facade, $mixed, string|null $case = null)
    {
    }
}
