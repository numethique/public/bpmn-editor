<?php

namespace App\Transformer\Strategies;

use App\Entity\Invitation;
use App\Facade\FacadeInterface;
use App\Facade\InvitationFacade;
use App\Transformer\TransformerInterface;

class InvitationTransformer implements TransformerInterface
{
    /**
     * @param Invitation $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new InvitationFacade();

        $facade->email = $mixed->getEmail();
        $facade->tokenId = $mixed->getToken();
        $facade->permissionAccess = $mixed->getProjectAccess();

        return $facade;
    }

    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        // TODO: Implement reverseTransform() method.
    }

}