<?php

namespace App\Transformer\Strategies;

use App\Entity\Comment;
use App\Facade\CommentFacade;
use App\Facade\FacadeInterface;
use App\Repository\FileRepository;
use App\Transformer\TransformerInterface;

class CommentTransformer implements TransformerInterface
{
    public function __construct(
        protected UserTransformer $authorTransformer,
        protected FileRepository  $fileRepository
    ) {}

    /**
     * @param Comment $mixed
     * @param string|null $case
     *
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new CommentFacade();

        $facade->id = $mixed->getUuid();
        $facade->fileId = $mixed->getFile()->getId();
        $facade->content = $mixed->getContent();
        $facade->createdAt = $mixed->getCreatedAt()->getTimestamp();
        $facade->lastChanged = $mixed->getUpdatedAt()->getTimestamp();
        $facade->author = $this->authorTransformer->transform($mixed->getAuthor());

        if (null !== $mixed->getReference()) {
            $facade->reference = $mixed->getReference();
        }

        return $facade;
    }

    /**
     * @param CommentFacade $facade
     * @param Comment $mixed
     * @param string|null $case
     *
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        $mixed->setContent($facade->content);
        $mixed->setUpdatedAt(new \DateTimeImmutable());

        if (self::CREATION === $case) {
            $mixed->setReference($facade->reference);
            $mixed->setFile($this->fileRepository->findOneByUuid($facade->fileId));
        }

        return $mixed;
    }
}
