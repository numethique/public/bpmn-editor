<?php

namespace App\Transformer\Strategies;

use App\Entity\Share;
use App\Facade\FacadeInterface;
use App\Facade\ShareFacade;
use App\Transformer\TransformerInterface;

class ShareTransformer implements TransformerInterface
{
    public function __construct(protected FileTransformer $fileTransformer)
    {}

    /**
     * @param Share $mixed
     * @param string|null $case
     * @return FacadeInterface
     */
    public function transform($mixed, ?string $case = null): FacadeInterface
    {
        $facade = new ShareFacade();

        $facade->id = $mixed->getUuid();

        $facade->file = $this->fileTransformer->transform($mixed->getFile(), $case);

        return $facade;
    }

    /**
     * @param ShareFacade $facade
     * @param Share $mixed
     * @param string|null $case
     * @return void
     */
    public function reverseTransform(FacadeInterface $facade, $mixed, ?string $case = null)
    {
        $mixed->setPassword($facade->password);
    }
}
