<?php

namespace App\Calculator;

use App\Entity\Folder;

class FilesAndFolderCalculator
{
    public function calculate(Folder $folder)
    {
        return [1 + $this->getChildrenCount($folder), $this->getFilesCount($folder)];
    }

    private function getChildrenCount(Folder $folder)
    {
        $childrenCount = $folder->getChildrens()->count();
        foreach ($folder->getChildrens() as $children) {
            $childrenCount += $this->getChildrenCount($children);
        }

        return $childrenCount;
    }

    private function getFilesCount(Folder $folder)
    {
        $filesCount = $folder->getFiles()->count();

        foreach ($folder->getChildrens() as $children) {
            $filesCount += $this->getFilesCount($children);
        }

        return $filesCount;
    }
}