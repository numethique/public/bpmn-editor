<?php

namespace App\EventSubscriber;

use App\Event\CommentEvent;
use Pusher\Pusher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CommentEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected Pusher $pusher
    ) {}

    public static function getSubscribedEvents()
    {
        return [
            CommentEvent::ADD => 'onCommentAdd',
            CommentEvent::UPDATE => 'onCommentUpdate',
            CommentEvent::DELETE => 'onCommentDelete',
        ];
    }

    public function onCommentAdd(CommentEvent $commentEvent)
    {
        $payload = $this->getEventPayload($commentEvent);
        $data = [
            'type' => 'COMMENT_ADDED',
            'payload' => json_encode($payload)
        ];
        $this->pusher->trigger('private-comments-' . $commentEvent->getComment()->getFile()->getUuid(), 'comment:add', $data);
    }

    public function onCommentUpdate(CommentEvent $commentEvent)
    {
        $payload = $this->getEventPayload($commentEvent);
        $data = [
            'type' => 'COMMENT_EDITED',
            'payload' => json_encode($payload)
        ];
        $this->pusher->trigger('private-comments-' . $commentEvent->getComment()->getFile()->getUuid(), 'comment:edit', $data);
    }

    public function onCommentDelete(CommentEvent $commentEvent)
    {
        $payload = $this->getEventPayload($commentEvent);
        $data = [
            'type' => 'COMMENT_REMOVED',
            'payload' => json_encode($payload)
        ];
        $this->pusher->trigger('private-comments-' . $commentEvent->getComment()->getFile()->getUuid(), 'comment:remove', $data);
    }

    /**
     * @param CommentEvent $commentEvent
     * @return array
     */
    public function getEventPayload(CommentEvent $commentEvent): array
    {
        $payload = [
            "id" => $commentEvent->getComment()->getUuid(),
            "fileId" => $commentEvent->getComment()->getFile()->getUuid(),
            "content" => $commentEvent->getComment()->getContent(),
            "created" => $commentEvent->getComment()->getCreatedAt()->getTimestamp(),
            "lastChanged" => $commentEvent->getComment()->getUpdatedAt()->getTimestamp(),
            "author" => [
                "id" => $commentEvent->getComment()->getAuthor()->getUuid(),
                "name" => $commentEvent->getComment()->getAuthor()->getName(),
                "username" => $commentEvent->getComment()->getAuthor()->getName(),
            ],
            "originAppInstanceId" => $commentEvent->getOriginAppInstanceId()
        ];
        if (null !== $commentEvent->getComment()->getReference()) {
            $payload["reference"] = $commentEvent->getComment()->getReference();
        }
        return $payload;
    }
}