<?php

namespace App\EventSubscriber;

use App\Event\FileEvent;
use Pusher\Pusher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UpdateFileEventSubscriber implements EventSubscriberInterface
{
    private $eventToSend = [];

    public function __construct(
        protected Pusher $pusher
    ) {}

    /**
     * @return string[]
     */
    public static function getSubscribedEvents()
    {
        return [
            FileEvent::UPDATE => 'onFileUpdate',
            FileEvent::ADD_ATTENTION => 'onAddAttention',
            FileEvent::DELETE_ATTENTION => 'onDeleteAttention',
            KernelEvents::TERMINATE => 'sendEvents',
        ];
    }

    public function onFileUpdate(FileEvent $fileEvent): void
    {
        $payload = [
            "fileId" => $fileEvent->getFile()->getUuid(),
            "timestamp" => $fileEvent->getFile()->getUpdatedAt()->getTimestamp(),
            "user" => [
                "id" => $fileEvent->getUser()->getUuid(),
                "name" => $fileEvent->getUser()->getName(),
                "username" => $fileEvent->getUser()->getName(),
            ],
            "revision" => $fileEvent->getFile()->getRevision(),
            "originAppInstanceId" => $fileEvent->getFile()->getOriginAppInstanceId()
        ];
        $data = [
            'type' => 'DIAGRAM_LAST_MODIFIED',
            'payload' => json_encode($payload)
        ];

        $this->eventToSend[] = [
            'channels' => 'private-diagram-update-' . $fileEvent->getFileId(),
            'event' => 'diagram:update',
            'data' => $data
        ];
    }

    public function onAddAttention(FileEvent $fileEvent)
    {
        $payload = [
            "coordinates" => $fileEvent->getCoordinates(),
            "color" => $fileEvent->getColor(),
            "authorUserId" => $fileEvent->getUser()->getUuid(),
        ];
        $data = [
            'type' => 'DIAGRAM_ATTENTION',
            'payload' => json_encode($payload)
        ];
        $this->pusher->trigger('private-diagram-attention-' . $fileEvent->getFileId(), 'diagram:attention:create', $data);
    }

    public function onDeleteAttention(FileEvent $fileEvent)
    {
        $payload = [
            "authorUserId" => $fileEvent->getUser()->getUuid(),
        ];
        $data = [
            'type' => 'DIAGRAM_ATTENTION',
            'payload' => json_encode($payload)
        ];

        $this->pusher->trigger('private-diagram-attention-' . $fileEvent->getFileId(), 'diagram:attention:delete', $data);
    }

    public function sendEvents(KernelEvent $kernelEvent)
    {
        foreach ($this->eventToSend as $eventToSend) {
            $this->pusher->trigger($eventToSend['channels'], $eventToSend['event'], $eventToSend['data']);
        }
    }
}
