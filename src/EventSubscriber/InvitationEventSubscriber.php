<?php

namespace App\EventSubscriber;

use App\Event\InvitationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class InvitationEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected MailerInterface $mailer,
        protected Environment $twig,
        protected $senderEmail,
        protected $applicationName
    ) {}

    public static function getSubscribedEvents()
    {
        return [
            InvitationEvent::CREATE => 'sendInvitation'
        ];
    }

    public function sendInvitation(InvitationEvent $invitationEvent)
    {
        $message = new Email();
        $message->to($invitationEvent->getInvitation()->getEmail());
        $message->from(new Address($this->senderEmail, $this->applicationName));
        $message->subject('Invitation to edit projects');
        $message->html($this->twig->render('mail/invitation.html.twig', [
            'invitation' => $invitationEvent->getInvitation()
        ]));

        $this->mailer->send($message);
    }
}
