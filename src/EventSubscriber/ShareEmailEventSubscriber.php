<?php

namespace App\EventSubscriber;

use App\Event\ShareEmailEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class ShareEmailEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected MailerInterface $mailer,
        protected Environment $twig,
        protected $senderEmail,
        protected $applicationName
    ) {}

    public static function getSubscribedEvents()
    {
        return [
            ShareEmailEvent::SEND_MAIL => 'sendShareEmail'
        ];
    }

    public function sendShareEmail(ShareEmailEvent $shareEmailEvent)
    {
        $message = new Email();
        $message->to($shareEmailEvent->getEmail());
        $message->from(new Address($this->senderEmail, $this->applicationName));
        $message->subject('Diagram shared');
        $message->html($this->twig->render('mail/share_email.html.twig', [
            'share' => $shareEmailEvent->getShare(),
            'message' => $shareEmailEvent->getMessage()
        ]));

        $this->mailer->send($message);
    }
}
