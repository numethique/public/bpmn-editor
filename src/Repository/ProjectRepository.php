<?php

namespace App\Repository;

use App\Entity\Organization;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Project>
 *
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findByUserAndOrganization(User $user, Organization $organization)
    {
        return $this->createQueryBuilder('p')
            ->join('p.organization', 'o')
            ->join('p.projectCollaborators', 'pc')
            ->join('pc.user', 'u')
            ->where('o.id = :oid')
            ->andWhere('u.id = :uid')
            ->setParameter('oid', $organization->getId())
            ->setParameter('uid', $user->getId())
            ->getQuery()
            ->getResult()
        ;
    }
}
