<?php

namespace App\Repository;

use App\Entity\Invitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Invitation>
 *
 * @method Invitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invitation[]    findAll()
 * @method Invitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invitation::class);
    }

    public function findByEmailAndProjectId($email, $projectId)
    {
        return $this->createQueryBuilder('i')
            ->join('i.project', 'p')
            ->where('i.email = :email')
            ->andWhere('p.uuid = :pid')
            ->setParameter('email', $email)
            ->setParameter('pid', $projectId)
            ->getQuery()
            ->getResult()
        ;
    }
}
