<?php

namespace App\Repository;

use App\Entity\UserOrganization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserOrganization>
 *
 * @method UserOrganization|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOrganization|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOrganization[]    findAll()
 * @method UserOrganization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOrganizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserOrganization::class);
    }

    public function findOneByEmailAndOrganizationId($email, $organizationId): UserOrganization
    {
        return $this->createQueryBuilder('uo')
            ->join('uo.organization', 'o')
            ->join('uo.user', 'u')
            ->where('o.uuid = :oid')
            ->andWhere('u.email = :email')
            ->setParameter('oid', $organizationId)
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findByOrganizationIdWithAdminRights($organizationId): array
    {
        return $this->createQueryBuilder('uo')
            ->join('uo.organization', 'o')
            ->where('o.uuid = :oid')
            ->andWhere('uo.permission = :permission')
            ->setParameter('oid', $organizationId)
            ->setParameter('permission', UserOrganization::ADMIN)
            ->getQuery()
            ->getResult()
            ;
    }
}
