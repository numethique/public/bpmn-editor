<?php

namespace App\Repository;

use App\Entity\ProjectUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProjectUser>
 *
 * @method ProjectUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectUser[]    findAll()
 * @method ProjectUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectUser::class);
    }

    public function findOneByEmailAndProjectId($email, $projectId): ?ProjectUser
    {
        return $this->createQueryBuilder('pu')
            ->join('pu.project', 'p')
            ->join('pu.user', 'u')
            ->where('p.uuid = :pid')
            ->andWhere('u.email = :email')
            ->setParameter('pid', $projectId)
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findOneByUserIdAndInternalProjectId($userId, $internalProjectId): ?ProjectUser
    {
        return $this->createQueryBuilder('pu')
            ->join('pu.project', 'p')
            ->join('pu.user', 'u')
            ->where('p.id = :pid')
            ->andWhere('u.id = :uid')
            ->setParameter('pid', $internalProjectId)
            ->setParameter('uid', $userId)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
