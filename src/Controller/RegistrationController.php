<?php

namespace App\Controller;

use App\Entity\Invitation;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\InvitationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Uid\UuidV1;

class RegistrationController extends AbstractController
{
    public function __construct(
        protected UserPasswordHasherInterface $userPasswordHasher,
        protected EntityManagerInterface $entityManager
    ) {}
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, InvitationRepository $invitationRepository): Response
    {
        $invitationId = $request->get('invitationToken');
        $projectId = $request->get('projectId');
        $invitation = $invitationRepository->findOneByToken($invitationId);

        $user = new User();
        $user->setUuid(UuidV1::generate());
        if ($invitation instanceof Invitation) {
            $user->setEmail($invitation->getEmail());
        }
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $this->userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $invitationId = $request->get('invitationToken');
            $projectId = $request->get('projectId');

            if (null !== $invitationId && null !== $projectId) {
                return $this->redirectToRoute('app_project_joinproject', [
                    'projectId' => $projectId,
                    'inviteToken' => $invitationId
                ]);
            }

            return $this->redirectToRoute('app_organization_new');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
