<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\UserOrganization;
use App\Form\OrganizationType;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Uid\UuidV1;

#[Route('/organization')]
class OrganizationController extends AbstractController
{
    #[Route('/new', name: 'app_organization_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $organization = new Organization();
        $organization->setUuid(UuidV1::generate());
        $userOrganization = new UserOrganization();
        $userOrganization->setOrganization($organization);
        $userOrganization->setUser($this->getUser());
        $userOrganization->setPermission(UserOrganization::ADMIN);
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($organization);
            $entityManager->persist($userOrganization);
            $entityManager->flush();

            return $this->redirectToRoute('app_homepage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('organization/new.html.twig', [
            'organization' => $organization,
            'form' => $form,
        ]);
    }
}
