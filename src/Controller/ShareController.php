<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\User;
use App\Transformer\Strategies\OrganizationTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShareController extends AbstractController
{
    #[Route('/share/{shareId}')]
    public function index(): Response
    {
        return $this->render('homepage/index.html.twig', [
            'data' =>  [
                "user" => 'null'
            ],
            'config' => [
                "pusher" => [
                    "key" => $this->getParameter("app.pusher_key")
                ],
                "sentry" => [
                    "enabled" => $this->getParameter('app.sentry_enabled'),
                    "key" => $this->getParameter('app.sentry_dsn'),
                    "environment" => "prod"
                ],
                "theme" => [
                    "colors" => [
                        "primary" => "#6a67ce",
                        "secondary" => "#00bfa5",
                        "accent" => "#332b5c"
                    ],
                    "logoPath" => "/img/cawemo.min.svg"
                ],
                "product" => [
                    "context" => "saas",
                    "isEnterprise" => true,
                    "isSaas" => true
                ],
                "mixpanel" => [
                    "token" => "",
                    "enabled" => false
                ],
                "features" => [
                    "assetRefreshEnabled" => false
                ]
            ]
        ]);
    }
}
