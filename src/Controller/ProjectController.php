<?php

namespace App\Controller;

use App\Entity\Invitation;
use App\Entity\Project;
use App\Entity\ProjectUser;
use App\Entity\User;
use App\Entity\UserOrganization;
use App\Repository\InvitationRepository;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/projects', methods: 'GET')]
class ProjectController extends AbstractController
{
    #[Route('/{projectId}/join', methods: 'GET')]
    public function joinProject($projectId, Request $request, EntityManagerInterface $manager, InvitationRepository $invitationRepository, ProjectRepository $projectRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Project $project */
        $project = $projectRepository->findOneByUuid($projectId);
        /** @var Invitation $invitation */
        $invitation = $invitationRepository->findOneByToken($request->get('inviteToken'));

        if (!$user instanceof User) {
            return $this->render('project/join.html.twig', [
                'invitation' => $invitation,
                'project' => $project
            ]);
        }

        if (!$invitation instanceof Invitation) {
            return $this->redirectToRoute('app_homepage_index');
        }

        if ($user->getEmail() == $invitation->getEmail() && $projectId == $invitation->getProject()->getUuid()) {
            $projectUser = new ProjectUser();
            $projectUser->setUser($user);
            $projectUser->setProject($project);
            $projectUser->setPermission($invitation->getProjectAccess());
            $manager->persist($projectUser);
            $userOrganization = new UserOrganization();
            $userOrganization->setUser($user);
            $userOrganization->setOrganization($project->getOrganization());
            $userOrganization->setPermission($invitation->getProjectAccess());
            $manager->persist($userOrganization);
        }

        $manager->remove($invitation);
        $manager->flush();

        return $this->redirectToRoute('app_homepage_index');
    }

}