<?php

namespace App\Controller;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use Pusher\Pusher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PusherController extends AbstractController
{
    #[Route('/pusher/auth', methods: 'POST')]
    public function auth(Request $request, LoggerInterface $logger, Pusher $pusher)
    {
        $socketId = $request->get('socket_id');
        $channelNames = $request->get('channel_name');

        /** @var User $user */
        $user = $this->getUser();
        $userInfo = [
            "user_id" => $user->getUuid(),
            "user_info" => [
                "userId" => $user->getUuid(),
                "fullName" => $user->getName(),
                "email" => $user->getEmail()
            ]
        ];

        $socketAuth = [];

        foreach ($channelNames as $channelName) {
            $authInfo = json_decode($pusher->socket_auth($channelName, $socketId, json_encode($userInfo)), true);

            $logger->error(json_encode($authInfo));

            $socketAuth[$channelName] =  [
                "status" => 200,
                "data" => $authInfo
            ];
        }

        return new JsonResponse($socketAuth);
    }
}
