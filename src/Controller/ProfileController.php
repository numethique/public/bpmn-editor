<?php

namespace App\Controller;

use App\Repository\InvitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profile')]
    public function display(InvitationRepository $invitationRepository)
    {
        $user = $this->getUser();

        $invitations = $invitationRepository->findByEmail($user->getEmail());

        return $this->render('profile/display.html.twig', [
            'user' => $user,
            'invitations' => $invitations
        ]);
    }
}