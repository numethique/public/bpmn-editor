<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\User;
use App\Transformer\Strategies\OrganizationTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    #[Route('/')]
    #[Route('/settings')]
    #[Route('/settings/members')]
    #[Route('/folders/{folderSlug}')]
    #[Route('/projects/{folderSlug}')]
    #[Route('/diagrams/{diagramSlug}')]
    #[Route('/milestones/{milestoneSlug}')]
    public function index(OrganizationTransformer $organizationTransformer): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $organizations = $user->getOrganizations();

        if (count($organizations) == 0) {
            return $this->redirectToRoute('app_invitation_list');
        }

        $organizationsFacade = [];
        foreach ($organizations as $organization) {
            $organizationsFacade[] = $organizationTransformer->transform($organization);
        }

        return $this->render('homepage/index.html.twig', [
            'data' =>  [
                "user" => [
                    "organizations" => $organizationsFacade,
                    "id" => $user->getUuid(),
                    "name" => $user->getName(),
                    "email" => $user->getEmail(),
                    "username" => $user->getName(),
                    "verified" => true,
                    "segment" => "C"
                ]
            ],
            'config' => [
                "pusher" => [
                    "key" => $this->getParameter("app.pusher_key")
                ],
                "sentry" => [
                    "enabled" => $this->getParameter('app.sentry_enabled'),
                    "key" => $this->getParameter('app.sentry_dsn'),
                    "environment" => "prod"
                ],
                "theme" => [
                    "colors" => [
                        "primary" => "#6a67ce",
                        "secondary" => "#00bfa5",
                        "accent" => "#332b5c"
                    ],
                    "logoPath" => "/img/cawemo.min.svg"
                ],
                "product" => [
                    "context" => "saas",
                    "isEnterprise" => false,
                    "isSaas" => true
                ],
                "mixpanel" => [
                    "token" => "",
                    "enabled" => false
                ],
                "features" => [
                    "assetRefreshEnabled" => false
                ]
            ]
        ]);
    }
}
