<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\InvitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/invitation')]
class InvitationController extends AbstractController
{
    #[Route('')]
    public function list(InvitationRepository $invitationRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        $invitations = $invitationRepository->findByEmail($user->getEmail());

        if (count($invitations) == 0) {
            return $this->redirectToRoute('app_organization_new');
        }

        return $this->render('invitation/list.html.twig', [
            'invitations' => $invitations,
        ]);
    }
}