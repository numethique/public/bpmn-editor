<?php

namespace App\Controller\InternalApi;

use App\Entity\Comment;
use App\Entity\File;
use App\Entity\Milestone;
use App\Event\FileEvent;
use App\Facade\DownloadFileFacade;
use App\Facade\FileFacade;
use App\Facade\MilestoneFacade;
use App\Facade\MoveFileFacade;
use App\Manager\DownloadManager;
use App\Repository\CommentRepository;
use App\Repository\FileRepository;
use App\Repository\FolderRepository;
use App\Security\FileVoter;
use App\Transformer\Strategies\FileTransformer;
use App\Transformer\Strategies\MilestoneTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;
use ZipStream\ZipStream;

#[Route('/internal-api/files')]
class FileController extends AbstractController
{
    public function __construct(
        protected FileTransformer $fileTransformer,
        protected FileRepository $fileRepository,
        protected EventDispatcherInterface $dispatcher
    ) {}

    #[Route('/{fileId}', methods: 'GET')]
    public function getFiles($fileId)
    {
        $file = $this->fileRepository->findOneByUuid($fileId);
        if (!$this->isGranted(FileVoter::VIEW, $file)) {
            throw new AccessDeniedException();
        }

        return new JsonResponse([
            "data" => [
                "file" => $this->fileTransformer->transform($file, FileTransformer::ONLY_FILE),
            ]
        ]);
    }

    #[Route('/duplicate', methods: 'POST')]
    public function duplicateFiles(Request $request,FolderRepository $folderRepository, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $fileIds  = json_decode($request->getContent(), true);

        $filesFacade = [];

        foreach ($fileIds as $fileId) {
            /** @var File $file */
            $file = $this->fileRepository->findOneByUuid($fileId);
            if (!$this->isGranted(FileVoter::EDIT, $file)) {
                throw new AccessDeniedException();
            }

            $duplicate = clone $file;
            $duplicate->setUuid(UuidV1::generate());
            $duplicate->setName($file->getName() . ' - Copy');
            $duplicate->setAuthor($this->getUser());
            $duplicate->setRevision(1);

            $manager->persist($duplicate);
            $filesFacade[] = $this->fileTransformer->transform($duplicate);
        }

        $manager->flush();

        return new JsonResponse([
            "data" => [
                "files" => $filesFacade
            ]
        ]);
    }

    #[Route('/download', methods: 'POST')]
    public function downloadFiles(Request $request, DownloadManager $manager, SerializerInterface $serializer)
    {
        $downloadFileFacade  = $serializer->deserialize($request->getContent(), DownloadFileFacade::class, 'json');

        $zip = new ZipStream(
            outputName: 'bpmn-editor.zip'
        );

        $manager->execute($downloadFileFacade, $zip);

        $zip->finish();

        return new Response();
    }

    #[Route('/move', methods: 'POST')]
    public function moveFiles(Request $request,FolderRepository $folderRepository, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        /** @var MoveFileFacade $moveFileFacade */
        $moveFileFacade  = $serializer->deserialize($request->getContent(), MoveFileFacade::class, 'json');

        $targetFolder = $folderRepository->findOneByUuid($moveFileFacade->targetFolderId);

        foreach ($moveFileFacade->fileIds as $fileId) {
            /** @var File $file */
            $file = $this->fileRepository->findOneByUuid($fileId);
            if (!$this->isGranted(FileVoter::EDIT, $file)) {
                throw new AccessDeniedException();
            }
            $file->setFolder($targetFolder);
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('', methods: 'POST')]
    public function createFiles(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $file = new File();

        $file->setUuid(UuidV1::generate());
        $file->setAuthor($this->getUser());

        $fileFacade = $serializer->deserialize($request->getContent(), FileFacade::class, 'json');

        $this->fileTransformer->reverseTransform($fileFacade, $file, FileTransformer::CREATION);

        if (!$this->isGranted(FileVoter::EDIT, $file)) {
            throw new AccessDeniedException();
        }

        $manager->persist($file);
        $manager->flush();

        return new JsonResponse([
            "data" => [
                "file" => $this->fileTransformer->transform($file, null),
            ]
        ]);
    }

    #[Route('', methods: 'DELETE')]
    public function deleteFiles(Request $request, EntityManagerInterface $manager)
    {
        $filesToDelete = json_decode($request->getContent(), true);

        foreach ($filesToDelete as $fileId) {
            $file = $this->fileRepository->findOneByUuid($fileId);
            if (!$this->isGranted(FileVoter::EDIT, $file)) {
                throw new AccessDeniedException();
            }
            $manager->remove($file);
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{fileId}/attention', methods: 'POST')]
    public function addAttention($fileId, Request $request)
    {
        $requestContent = json_decode($request->getContent(), true);

        $this->dispatcher->dispatch(FileEvent::addAttention($this->getUser(), $requestContent['coordinates'], $requestContent['color'], $fileId), FileEvent::ADD_ATTENTION);

        return new JsonResponse();
    }

    #[Route('/{fileId}/attention', methods: 'DELETE')]
    public function deleteAttention($fileId)
    {
        $this->dispatcher->dispatch(FileEvent::deleteAttention($this->getUser(), $fileId), FileEvent::DELETE_ATTENTION);

        return new JsonResponse();
    }

    #[Route('/{fileId}', methods: 'PATCH')]
    public function patchFile($fileId, Request $request, EntityManagerInterface $manager, SerializerInterface $serializer)
    {
        /** @var File $file */
        $file = $this->fileRepository->findOneByUuid($fileId);
        if (!$this->isGranted(FileVoter::EDIT, $file)) {
            throw new AccessDeniedException();
        }

        $fileFacade = $serializer->deserialize($request->getContent(), FileFacade::class, 'json');

        $this->fileTransformer->reverseTransform($fileFacade, $file);

        $manager->flush();

        $this->dispatcher->dispatch(FileEvent::updateFile($file, $this->getUser()), FileEvent::UPDATE);

        return new JsonResponse([
            "data" => [
                "file" => $this->fileTransformer->transform($file),
            ]
        ]);
    }

    #[Route('/{fileId}/links')]
    public function getLinks($fileId)
    {
        return new JsonResponse([]);
    }

    #[Route('/{fileId}/related')]
    public function getRelated($fileId)
    {
        return new JsonResponse(['data' => []]);
    }

    #[Route('/{fileId}/milestones/autosave', methods: 'POST')]
    public function autosaveMilestones($fileId, MilestoneTransformer $milestoneTransformer, EntityManagerInterface $manager)
    {
        /** @var File $file */
        $file = $this->fileRepository->findOneByUuid($fileId);
        if (!$this->isGranted(FileVoter::VIEW, $file)) {
            throw new AccessDeniedException();
        }

        //TODO add check if content is same return empty response
        $milestone = new Milestone();
        $milestone->setUuid(UuidV1::generate());
        $milestone->setAuthor($this->getUser());

        $milestoneFacade = new MilestoneFacade();
        $milestoneFacade->fileId = $fileId;
        $milestoneFacade->name = 'Autosaved during restore';

        $milestoneTransformer->reverseTransform($milestoneFacade, $milestone);

        $manager->persist($milestone);
        $manager->flush();

        return new JsonResponse([
            'data' => [
                'milestone' => $milestoneTransformer->transform($milestone)
            ]
        ]);
    }

    #[Route('/{fileId}/milestones', methods: 'GET')]
    public function getMilestones($fileId, MilestoneTransformer $milestoneTransformer)
    {
        /** @var File $file */
        $file = $this->fileRepository->findOneByUuid($fileId);
        if (!$this->isGranted(FileVoter::VIEW, $file)) {
            throw new AccessDeniedException();
        }

        $milestonesFacade = [];

        foreach ($file->getMilestones() as $milestone) {
            $milestonesFacade[] = $milestoneTransformer->transform($milestone);
        }

        return new JsonResponse([
            'data' => [
                'milestones' => $milestonesFacade
            ]
        ]);
    }

    #[Route('/{fileId}/node/{reference}', methods: 'DELETE')]
    public function deleteElement($fileId, $reference, CommentRepository $commentRepository, EntityManagerInterface $manager)
    {
        $comments = $commentRepository->findByFileAndReference($fileId, $reference);

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            if ($comment->getAuthor() == $this->getUser()) {
                $manager->remove($comment);
            }
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
