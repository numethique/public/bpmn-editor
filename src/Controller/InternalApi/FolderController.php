<?php

namespace App\Controller\InternalApi;

use App\Entity\Folder;
use App\Facade\FolderFacade;
use App\Repository\FolderRepository;
use App\Transformer\Strategies\FolderTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;

#[Route('/internal-api/folders')]
class FolderController extends AbstractController
{
    public function __construct(
        protected FolderRepository $folderRepository,
        protected FolderTransformer $folderTransformer
    ) {}

    #[Route('/{folderId}', methods: 'GET')]
    public function folderDetail($folderId)
    {
        $folder = $this->folderRepository->findOneByUuid($folderId);

        return new JsonResponse([
            "data" => $this->folderTransformer->transform($folder)
        ]);
    }

    #[Route('/{folderId}', methods: 'PATCH')]
    public function updateFolder($folderId, Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $folder = $this->folderRepository->findOneByUuid($folderId);

        $folderFacade = $serializer->deserialize($request->getContent(), FolderFacade::class, 'json');

        $this->folderTransformer->reverseTransform($folderFacade, $folder);

        $manager->flush();

        return new JsonResponse([
            "data" => $this->folderTransformer->transform($folder)
        ]);
    }

    #[Route('/{folderId}', methods: 'DELETE')]
    public function deleteFolder($folderId, EntityManagerInterface $manager)
    {
        $folder = $this->folderRepository->findOneByUuid($folderId);

        $manager->remove($folder);
        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('', methods: 'POST')]
    public function createFolder(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $folder = new Folder();

        $folder->setUuid(UuidV1::generate());
        $folder->setCreatedBy($this->getUser());

        $folderFacade = $serializer->deserialize($request->getContent(), FolderFacade::class, 'json');

        $this->folderTransformer->reverseTransform($folderFacade, $folder, FolderTransformer::CREATION);

        $manager->persist($folder);
        $manager->flush();

        return new JsonResponse([
            "data" => $this->folderTransformer->transform($folder)
        ]);
    }
}
