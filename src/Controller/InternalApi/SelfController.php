<?php

namespace App\Controller\InternalApi;

use App\Transformer\Strategies\SelfTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/internal-api/self')]
class SelfController extends AbstractController
{
    public function __construct(
        protected SelfTransformer $selfTransformer,
    ) {}

    #[Route('')]
    public function self()
    {
        return new JsonResponse([
            "data" => $this->selfTransformer->transform($this->getUser())
        ]);
    }
}
