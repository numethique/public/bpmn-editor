<?php

namespace App\Controller\InternalApi;

use App\Calculator\FilesAndFolderCalculator;
use App\Entity\Folder;
use App\Repository\FileRepository;
use App\Repository\FolderRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/internal-api/dry-run')]
class DryRunController
{
    public function __construct(
        protected FileRepository $fileRepository,
        protected FolderRepository $folderRepository
    ) {}

    #[Route('/delete')]
    public function delete(Request $request, FilesAndFolderCalculator $calculator): JsonResponse
    {
        $folderId = $request->get('folderId', null);

        if (null !== $folderId) {
            /** @var Folder $folder */
            $folder = $this->folderRepository->findOneByUuid($folderId);

            list($folderCount, $fileCount) = $calculator->calculate($folder);

            return new JsonResponse(['data' => [
                'folderCount' => $folderCount,
                'fileCount' => $fileCount,
                'warnings' => []
            ]]);
        }

        $fileId = $request->get('fileId');
        $files = $this->fileRepository->findByUuid($fileId);

        return new JsonResponse(['data' => [
            'folderCount' => 0,
            'fileCount' => count($files),
            'warnings' => []
        ]]);
    }

    #[Route('/move')]
    public function move(): JsonResponse
    {
        return new JsonResponse(['data' => [
            'warnings' => []
        ]]);
    }
}
