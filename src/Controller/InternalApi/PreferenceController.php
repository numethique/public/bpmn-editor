<?php

namespace App\Controller\InternalApi;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/internal-api/preferences')]
class PreferenceController
{

    #[Route('')]
    public function preferences(): Response
    {
        return new JsonResponse([
            "data" => [
                "onboardingFlowSeen" => false,
                "showNewsletterConsentRequest" => true
            ]
        ]);
    }
}