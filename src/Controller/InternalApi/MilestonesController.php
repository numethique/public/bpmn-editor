<?php

namespace App\Controller\InternalApi;

use App\Entity\Milestone;
use App\Facade\MilestoneFacade;
use App\Repository\MilestoneRepository;
use App\Transformer\Strategies\MilestoneTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;

#[Route('/internal-api/milestones')]
class MilestonesController extends AbstractController
{
    #[Route('', methods: 'POST')]
    public function create(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager, MilestoneTransformer $milestoneTransformer)
    {
        $milestone = new Milestone();
        $milestone->setUuid(UuidV1::generate());
        $milestone->setAuthor($this->getUser());

        $milestoneFacade = $serializer->deserialize($request->getContent(), MilestoneFacade::class, 'json');

        $milestoneTransformer->reverseTransform($milestoneFacade, $milestone);

        $manager->persist($milestone);
        $manager->flush();

        return new JsonResponse([
            'data' => [
                'milestone' => $milestoneTransformer->transform($milestone)
            ]
        ]);
    }

    #[Route('/{milestoneId}', methods: 'GET')]
    public function get($milestoneId, MilestoneRepository $milestoneRepository, MilestoneTransformer $milestoneTransformer)
    {
        $milestone = $milestoneRepository->findOneByUuid($milestoneId);

        return new JsonResponse([
            'data' => [
                'milestone' => $milestoneTransformer->transform($milestone)
            ]
        ]);
    }

    #[Route('/{milestoneId}', methods: 'PATCH')]
    public function update($milestoneId, Request $request, MilestoneRepository $milestoneRepository, MilestoneTransformer $milestoneTransformer, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $milestone = $milestoneRepository->findOneByUuid($milestoneId);

        $milestoneFacade = $serializer->deserialize($request->getContent(), MilestoneFacade::class, 'json');

        $milestoneTransformer->reverseTransform($milestoneFacade, $milestone, MilestoneTransformer::UPDATE);

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{milestoneId}/restore', methods: 'POST')]
    public function restore($milestoneId, Request $request, MilestoneRepository $milestoneRepository, MilestoneTransformer $milestoneTransformer, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        /** @var Milestone $milestone */
        $milestone = $milestoneRepository->findOneByUuid($milestoneId);

        $newMilestone = new Milestone();
        $newMilestone->setUuid(UuidV1::generate());
        $newMilestone->setAuthor($this->getUser());
        $newMilestone->setName($milestone->getName() . ' (restored)');
        $newMilestone->setFile($milestone->getFile());
        $newMilestone->setContent($milestone->getContent());

        $manager->persist($newMilestone);
        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{milestoneId}', methods: 'DELETE')]
    public function delete($milestoneId, MilestoneRepository $milestoneRepository, MilestoneTransformer $milestoneTransformer, EntityManagerInterface $manager)
    {
        $milestone = $milestoneRepository->findOneByUuid($milestoneId);

        $manager->remove($milestone);
        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}