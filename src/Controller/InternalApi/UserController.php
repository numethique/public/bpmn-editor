<?php

namespace App\Controller\InternalApi;

use App\Repository\UserRepository;
use App\Transformer\Strategies\UserTransformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/internal-api/users')]
class UserController
{
    public function __construct(
        protected UserRepository $userRepository,
        protected UserTransformer $userTransformer,
    ) {}

    #[Route('')]
    public function users()
    {
        $users = $this->userRepository->findAll();

        $usersFacade = [];

        foreach ($users as $user) {
            $usersFacade[] = $this->userTransformer->transform($user);
        }

        return new JsonResponse([
            "data" => $usersFacade
        ]);
    }
}
