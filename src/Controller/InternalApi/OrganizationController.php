<?php

namespace App\Controller\InternalApi;

use App\Entity\Organization;
use App\Entity\ProjectUser;
use App\Facade\OrganizationFacade;
use App\Facade\UserFacade;
use App\Repository\OrganizationRepository;
use App\Repository\ProjectRepository;
use App\Repository\ProjectUserRepository;
use App\Repository\UserOrganizationRepository;
use App\Transformer\Strategies\OrganizationTransformer;
use App\Transformer\Strategies\ProjectTransformer;
use App\Transformer\Strategies\UserTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/internal-api/organizations')]
class OrganizationController extends AbstractController
{
    public function __construct(
        protected OrganizationRepository $organizationRepository,
        protected ProjectTransformer $projectTransformer,
        protected ProjectRepository $projectRepository,
        protected OrganizationTransformer $organizationTransformer,
        protected UserTransformer $userTransformer,
    ) {}

    #[Route('/{organizationId}/projects')]
    public function projects($organizationId)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->findOneByUuid($organizationId);

        $projectsFacade = [];

        $projects = $this->projectRepository->findByUserAndOrganization($this->getUser(), $organization);

        foreach ($projects as $project) {
            $projectsFacade[] = $this->projectTransformer->transform($project);
        }

        return new JsonResponse([
            "data" => [
                "projects" => $projectsFacade
            ]
        ]);
    }

    #[Route('/{organizationId}/admin-projects')]
    public function adminProjects($organizationId)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->findOneByUuid($organizationId);

        $projects = $organization->getProjects();

        $projectsFacade = [];

        foreach ($projects as $project) {
            $projectsFacade[] = $this->projectTransformer->transform($project, OrganizationTransformer::ADMIN);
        }

        return new JsonResponse([
            "data" => [
                "projects" => $projectsFacade
            ]
        ]);
    }

    #[Route('/{organizationId}/collaborators', methods: 'GET')]
    #[Route('/{organizationId}/users', methods: 'GET')]
    public function users($organizationId)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->findOneByUuid($organizationId);

        $usersFacade = [];

        foreach ($organization->getUsers() as $user) {
            $usersFacade[] = $this->userTransformer->transform($user);
        }

        return new JsonResponse([
            "data" => $usersFacade
        ]);
    }

    #[Route('/{organizationId}/collaborators', methods: 'DELETE')]
    public function removeCollaborators($organizationId, Request $request, SerializerInterface $serializer, ProjectUserRepository $projectUserRepository, UserOrganizationRepository $userOrganizationRepository, EntityManagerInterface $manager)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->findOneByUuid($organizationId);

        $userFacade = $serializer->deserialize($request->getContent(), UserFacade::class, 'json');

        $userOrganization = $userOrganizationRepository->findOneByEmailAndOrganizationId($userFacade->email, $organizationId);

        $organization->removeUserOrganization($userOrganization);

        foreach ($organization->getProjects() as $project) {
            $projectUser = $projectUserRepository->findOneByEmailAndProjectId($userFacade->email, $project->getUuid());
            if ($projectUser instanceof ProjectUser) {
                $manager->remove($projectUser);
            }
        }

        $manager->flush();

        return new JsonResponse([
            "data" => $this->organizationTransformer->transform($organization)
        ]);
    }

    #[Route('/{organizationId}', methods: 'PATCH')]
    public function update($organizationId, Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->findOneByUuid($organizationId);

        $organizationFacade = $serializer->deserialize($request->getContent(), OrganizationFacade::class, 'json');

        $this->organizationTransformer->reverseTransform($organizationFacade, $organization);

        $manager->flush();

        return new JsonResponse([
            "data" => $this->organizationTransformer->transform($organization)
        ]);
    }

    #[Route('/{organizationId}/keys')]
    public function keys($organizationId)
    {
        return new JsonResponse([
            "data" => [
            ]
        ]);
    }
}