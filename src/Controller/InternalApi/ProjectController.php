<?php

namespace App\Controller\InternalApi;

use App\Entity\Invitation;
use App\Entity\Project;
use App\Entity\ProjectUser;
use App\Entity\User;
use App\Event\InvitationEvent;
use App\Facade\ProjectFacade;
use App\Facade\ProjectInvitationFacade;
use App\Facade\ProjectUserFacade;
use App\Facade\UserFacade;
use App\Manager\InvitationManager;
use App\Repository\InvitationRepository;
use App\Repository\ProjectRepository;
use App\Repository\ProjectUserRepository;
use App\Repository\UserRepository;
use App\Transformer\Strategies\InvitationTransformer;
use App\Transformer\Strategies\ProjectTransformer;
use App\Transformer\Strategies\ProjectUserTransformer;
use App\Transformer\Strategies\UserTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;

#[Route('/internal-api/projects')]
class ProjectController extends AbstractController
{
    public function __construct(
        protected ProjectRepository $projectRepository,
        protected ProjectTransformer $projectTransformer
    ) {}

    #[Route('/{projectId}', methods: 'GET')]
    public function projectDetail($projectId)
    {
        $project = $this->projectRepository->findOneByUuid($projectId);

        return new JsonResponse([
            "data" => $this->projectTransformer->transform($project)
        ]);
    }

    #[Route('', methods: 'POST')]
    public function createProject(Request $request, EntityManagerInterface $manager, SerializerInterface $serializer)
    {
        $project = new Project();

        $project->setUuid(UuidV1::generate());
        $projectUser = new ProjectUser();
        $projectUser->setUser($this->getUser());
        $projectUser->setProject($project);
        $projectUser->setPermission(ProjectUser::OWNER);
        $manager->persist($projectUser);

        $projectFacade = $serializer->deserialize($request->getContent(), ProjectFacade::class, 'json');

        $this->projectTransformer->reverseTransform($projectFacade, $project, ProjectTransformer::CREATION);

        $manager->persist($project);
        $manager->flush();

        return new JsonResponse([
            "data" => $this->projectTransformer->transform($project)
        ]);
    }

    #[Route('/{projectId}', methods: 'PATCH')]
    public function updateProject($projectId, Request $request, EntityManagerInterface $manager, SerializerInterface $serializer)
    {
        $project = $this->projectRepository->findOneByUuid($projectId);

        $projectFacade = $serializer->deserialize($request->getContent(), ProjectFacade::class, 'json');

        $this->projectTransformer->reverseTransform($projectFacade, $project, ProjectTransformer::UPDATE);

        $manager->flush();

        return new JsonResponse([
            "data" => $this->projectTransformer->transform($project)
        ]);
    }

    #[Route('', methods: 'DELETE')]
    public function deleteProjects(Request $request, EntityManagerInterface $manager)
    {
        $projectsToDelete = json_decode($request->getContent(), true);

        foreach ($projectsToDelete as $projectId) {
            $project = $this->projectRepository->findOneByUuid($projectId);
            $manager->remove($project);
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{projectId}/invite', methods: 'POST')]
    public function inviteToProject($projectId, Request $request, EntityManagerInterface $manager, SerializerInterface $serializer, InvitationManager $invitationManager, EventDispatcherInterface $dispatcher, InvitationTransformer $invitationTransformer)
    {
        $project = $this->projectRepository->findOneByUuid($projectId);

        $projectInvitationFacade = $serializer->deserialize($request->getContent(), ProjectInvitationFacade::class, 'json');

        $invitations = $invitationManager->handle($projectInvitationFacade, $project);
        $invitationsFacade = [];

        foreach ($invitations as $invitation) {
            $dispatcher->dispatch(InvitationEvent::create($invitation), InvitationEvent::CREATE);
            $manager->persist($invitation);
            $invitationsFacade[] = $invitationTransformer->transform($invitation);
        }

        $manager->flush();

        return new JsonResponse([
            "data" => [
                "invitationResults" => $invitationsFacade
            ]
        ]);
    }

    #[Route('/{projectId}/invitations/resend', methods: 'POST')]
    public function resendInviteToProject($projectId, Request $request, SerializerInterface $serializer, InvitationRepository $invitationRepository, EventDispatcherInterface $dispatcher)
    {
        $userToReinvite = $serializer->deserialize($request->getContent(), UserFacade::class, 'json');

        $invitations = $invitationRepository->findByEmailAndProjectId($userToReinvite->email, $projectId);

        foreach ($invitations as $invitation) {
            $dispatcher->dispatch(InvitationEvent::create($invitation), InvitationEvent::CREATE);
        }

        return new Response('', Response::HTTP_ACCEPTED);
    }

    #[Route('/{projectId}/collaborators', methods: 'GET')]
    public function getCollaborators($projectId, ProjectUserTransformer $projectUserTransformer)
    {
        /** @var Project $project */
        $project = $this->projectRepository->findOneByUuid($projectId);

        $usersFacade = [];

        foreach ($project->getProjectCollaborators() as $collaborator) {
            $usersFacade[] = $projectUserTransformer->transform($collaborator);
        }

        return new JsonResponse([
            "data" => $usersFacade
        ]);
    }

    #[Route('/{projectId}/collaborators', methods: 'DELETE')]
    public function deleteCollaborators($projectId, Request $request, SerializerInterface $serializer, ProjectUserRepository $projectUserRepository, InvitationRepository $invitationRepository, EntityManagerInterface $manager)
    {
        /** @var UserFacade $userFacade */
        $userFacade = $serializer->deserialize($request->getContent(), UserFacade::class, 'json');

        $projectUser = $projectUserRepository->findOneByEmailAndProjectId($userFacade->email, $projectId);
        if ($projectUser instanceof ProjectUser) {
            $manager->remove($projectUser);
        }

        $invitations = $invitationRepository->findByEmailAndProjectId($userFacade->email, $projectId);
        foreach ($invitations as $invitation) {
            $manager->remove($invitation);
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{projectId}/collaborators', methods: 'PATCH')]
    public function patchCollaborators($projectId, Request $request, SerializerInterface $serializer, ProjectUserRepository $projectUserRepository, InvitationRepository $invitationRepository, EntityManagerInterface $manager)
    {
        /** @var ProjectUserFacade $projectUserFacade */
        $projectUserFacade = $serializer->deserialize($request->getContent(), ProjectUserFacade::class, 'json');

        $projectUser = $projectUserRepository->findOneByEmailAndProjectId($projectUserFacade->email, $projectId);
        if ($projectUser instanceof ProjectUser) {
            $projectUser->setPermission($projectUserFacade->permissionAccess);
        }

        $invitations = $invitationRepository->findByEmailAndProjectId($projectUserFacade->email, $projectId);
        foreach ($invitations as $invitation) {
            $invitation->setProjectAccess($projectUserFacade->permissionAccess);
        }

        $manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
