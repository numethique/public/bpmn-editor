<?php

namespace App\Controller\InternalApi;


use App\Entity\Share;
use App\Event\ShareEmailEvent;
use App\Facade\ShareEmailFacade;
use App\Facade\ShareFacade;
use App\Repository\FileRepository;
use App\Repository\ShareRepository;
use App\Transformer\Strategies\ShareTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;

#[Route('/internal-api/shares')]
class ShareController
{
    public function __construct(
        protected EntityManagerInterface $manager,
        protected ShareRepository $shareRepository,
        protected SerializerInterface $serializer,
        protected ShareTransformer $shareTransformer
    ) {}

    #[Route('', methods: 'POST')]
    public function create(Request $request, FileRepository $fileRepository)
    {
        /** @var ShareFacade $shareFacade */
        $shareFacade  = $this->serializer->deserialize($request->getContent(), ShareFacade::class, 'json');

        $share = new Share();
        $share->setUuid(UuidV1::generate());

        $file = $fileRepository->findOneByUuid($shareFacade->fileId);

        $share->setFile($file);

        $this->manager->persist($share);
        $this->manager->flush();

        return new JsonResponse([
            'data' => [
                'share' => $this->shareTransformer->transform($share, ShareTransformer::SHARE)
            ]
        ]);
    }

    #[Route('/{shareId}', methods: 'GET')]
    public function show($shareId)
    {
        /** @var Share $share */
        $share = $this->shareRepository->findOneByUuid($shareId);

        if ('' !== $share->getPassword()) {
            return new JsonResponse([
                'errors' => [
                    [
                        'reason' => 'INVALID_PASSWORD'
                    ]
                ]
            ], Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse([
            'data' => [
                'share' => $this->shareTransformer->transform($share, ShareTransformer::DISPLAY_SHARE)
            ]
        ]);
    }

    #[Route('/{shareId}/authenticate', methods: 'POST')]
    public function authenticate($shareId, Request $request)
    {
        /** @var Share $share */
        $share = $this->shareRepository->findOneByUuid($shareId);

        /** @var ShareFacade $shareFacade */
        $shareFacade  = $this->serializer->deserialize($request->getContent(), ShareFacade::class, 'json');

        if ($shareFacade->password !== $share->getPassword()) {
            return new JsonResponse([
                'errors' => [
                    [
                        'reason' => 'INVALID_PASSWORD'
                    ]
                ]
            ], Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse([
            'data' => [
                'share' => $this->shareTransformer->transform($share, ShareTransformer::DISPLAY_SHARE)
            ]
        ]);
    }

    #[Route('/{shareId}/email', methods: 'POST')]
    public function email($shareId, Request $request, EventDispatcherInterface $dispatcher)
    {
        /** @var Share $share */
        $share = $this->shareRepository->findOneByUuid($shareId);

        /** @var ShareEmailFacade $shareEmailFacade */
        $shareEmailFacade  = $this->serializer->deserialize($request->getContent(), ShareEmailFacade::class, 'json');

        foreach ($shareEmailFacade->emails as $email) {
            $dispatcher->dispatch(ShareEmailEvent::create($share, $email, $shareEmailFacade->message), ShareEmailEvent::SEND_MAIL);
        }

        return new Response('', Response::HTTP_ACCEPTED);
    }

    #[Route('/{shareId}', methods: 'DELETE')]
    public function delete($shareId)
    {
        $share = $this->shareRepository->findOneByUuid($shareId);
        $this->manager->remove($share);
        $this->manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    #[Route('/{shareId}/password', methods: 'POST')]
    public function addPassword($shareId, Request $request)
    {
        $share = $this->shareRepository->findOneByUuid($shareId);

        /** @var ShareFacade $shareFacade */
        $shareFacade  = $this->serializer->deserialize($request->getContent(), ShareFacade::class, 'json');

        $this->shareTransformer->reverseTransform($shareFacade, $share);

        $this->manager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}