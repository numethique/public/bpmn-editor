<?php

namespace App\Controller\InternalApi;

use App\Entity\Comment;
use App\Entity\File;
use App\Event\CommentEvent;
use App\Facade\CommentFacade;
use App\Repository\CommentRepository;
use App\Repository\FileRepository;
use App\Security\FileVoter;
use App\Transformer\Strategies\CommentTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV1;

#[Route('/internal-api')]
class CommentController extends AbstractController
{
    public function __construct(
        protected CommentTransformer $commentTransformer,
        protected EventDispatcherInterface $dispatcher
    ) {}

    #[Route('/files/{fileId}/comments', methods: 'GET')]
    public function getComments($fileId, FileRepository $fileRepository)
    {
        /** @var File $file */
        $file = $fileRepository->findOneByUuid($fileId);
        if (!$this->isGranted(FileVoter::VIEW, $file)) {
            throw new AccessDeniedException();
        }

        $comments = [];

        foreach ($file->getComments() as $comment) {
            $comments[] = $this->commentTransformer->transform($comment);
        }

        return new JsonResponse([
            "data" => $comments
        ]);
    }

    #[Route('/comments', methods: 'POST')]
    public function addComment(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $comment = new Comment();

        $comment->setUuid(UuidV1::generate());
        $comment->setAuthor($this->getUser());

        $commentFacade = $serializer->deserialize($request->getContent(), CommentFacade::class, 'json');

        $this->commentTransformer->reverseTransform($commentFacade, $comment, CommentTransformer::CREATION);

        if (!$this->isGranted(FileVoter::COMMENT, $comment->getFile())) {
            throw new AccessDeniedException();
        }

        $manager->persist($comment);
        $manager->flush();

        $this->dispatcher->dispatch(CommentEvent::addComment($comment, $commentFacade), CommentEvent::ADD);

        return new JsonResponse([
            "data" =>$this->commentTransformer->transform($comment, null),
        ]);
    }

    #[Route('/comments/{commentId}', methods: 'PATCH')]
    public function updateComment($commentId, Request $request, SerializerInterface $serializer, EntityManagerInterface $manager, CommentRepository $commentRepository)
    {
        $comment = $commentRepository->findOneByUuid($commentId);

        if (!$this->isGranted(FileVoter::COMMENT, $comment->getFile())) {
            throw new AccessDeniedException();
        }

        $commentFacade = $serializer->deserialize($request->getContent(), CommentFacade::class, 'json');

        $this->commentTransformer->reverseTransform($commentFacade, $comment, CommentTransformer::UPDATE);

        $manager->flush();

        $this->dispatcher->dispatch(CommentEvent::updateComment($comment, $commentFacade), CommentEvent::UPDATE);

        return new JsonResponse([
            "data" =>$this->commentTransformer->transform($comment, null),
        ]);
    }

    #[Route('/comments/{commentId}', methods: 'DELETE')]
    public function deleteComment($commentId, Request $request, EntityManagerInterface $manager, CommentRepository $commentRepository)
    {
        $comment = $commentRepository->findOneByUuid($commentId);

        if (!$this->isGranted(FileVoter::COMMENT, $comment->getFile())) {
            throw new AccessDeniedException();
        }

        $requestContent = json_decode($request->getContent(), true);

        $this->dispatcher->dispatch(CommentEvent::deleteComment($comment, $requestContent['originAppInstanceId']), CommentEvent::DELETE);

        $manager->remove($comment);
        $manager->flush();

        return new JsonResponse();
    }
}
