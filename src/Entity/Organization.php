<?php

namespace App\Entity;

use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrganizationRepository::class)]
class Organization
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: Project::class, mappedBy: 'organization', orphanRemoval: true)]
    private Collection $projects;

    #[ORM\OneToMany(targetEntity: File::class, mappedBy: 'organization', orphanRemoval: true)]
    private Collection $files;

    #[ORM\Column(length: 255)]
    private ?string $uuid = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\OneToMany(targetEntity: UserOrganization::class, mappedBy: 'organization', orphanRemoval: true)]
    private Collection $userOrganizations;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->folders = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->userOrganizations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Project>
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): static
    {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
            $project->setOrganization($this);
        }

        return $this;
    }

    public function removeProject(Project $project): static
    {
        if ($this->projects->removeElement($project)) {
            // set the owning side to null (unless already changed)
            if ($project->getOrganization() === $this) {
                $project->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): static
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
            $file->setOrganization($this);
        }

        return $this;
    }

    public function removeFile(File $file): static
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getOrganization() === $this) {
                $file->setOrganization(null);
            }
        }

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, UserOrganization>
     */
    public function getUserOrganizations(): Collection
    {
        return $this->userOrganizations;
    }

    public function addUserOrganization(UserOrganization $userOrganization): static
    {
        if (!$this->userOrganizations->contains($userOrganization)) {
            $this->userOrganizations->add($userOrganization);
            $userOrganization->setOrganization($this);
        }

        return $this;
    }

    public function removeUserOrganization(UserOrganization $userOrganization): static
    {
        if ($this->userOrganizations->removeElement($userOrganization)) {
            // set the owning side to null (unless already changed)
            if ($userOrganization->getOrganization() === $this) {
                $userOrganization->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        $users = [];

        foreach ($this->getUserOrganizations() as $userOrganization) {
            $users[] = $userOrganization->getUser();
        }

        return $users;
    }
}
