<?php

namespace App\Entity;

use App\Repository\FolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FolderRepository::class)]
class Folder
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $uuid = null;

    #[ORM\OneToMany(targetEntity: File::class, mappedBy: 'folder', orphanRemoval: true)]
    private Collection $files;

    #[ORM\OneToMany(targetEntity: Folder::class, mappedBy: 'parent', orphanRemoval: true)]
    private ?Collection $childrens;

    #[ORM\ManyToOne(targetEntity: Folder::class, inversedBy: 'childrens')]
    private ?Folder $parent = null;

    #[ORM\ManyToOne(inversedBy: 'folders')]
    private ?Project $project = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'folders')]
    private ?User $createdBy = null;

    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->childrens = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return File[]
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

    public function getParent(): ?Folder
    {
        return $this->parent;
    }

    public function setParent(Folder $parent): void
    {
        $this->parent = $parent;
    }

    public function getChildrens()
    {
        return $this->childrens;
    }

    public function addChildren(Folder $folder)
    {
        $this->childrens->add($folder);
        $folder->setParent($this);
    }

    public function removeChildren(Folder $folder)
    {
        $this->childrens->removeElement($folder);
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
