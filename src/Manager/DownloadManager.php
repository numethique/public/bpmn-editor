<?php

namespace App\Manager;

use App\Entity\File;
use App\Entity\Folder;
use App\Facade\DownloadFileFacade;
use App\Repository\FileRepository;
use App\Repository\FolderRepository;
use App\Security\FileVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use ZipStream\ZipStream;

class DownloadManager
{
    private $zip;

    public function __construct(
        protected FileRepository $fileRepository,
        protected FolderRepository $folderRepository,
        protected AuthorizationCheckerInterface $authorizationChecker
    ) {}

    public function execute(DownloadFileFacade $downloadFileFacade, ZipStream $zip): void
    {
        $this->zip = $zip;

        foreach ($downloadFileFacade->fileIds as $fileId) {
            /** @var File $file */
            $file = $this->fileRepository->findOneByUuid($fileId);

            $this->addFileToZip($file);
        }

        foreach ($downloadFileFacade->folderIds as $folderId) {
            /** @var Folder $folder */
            $folder = $this->folderRepository->findOneByUuid($folderId);
            $this->addFolderContent($folder);
        }
    }

    protected function addFolderContent(Folder $folder, $folderPrefix = ''): void
    {
        $filePrefix = $folderPrefix . $folder->getName() . '/';
        foreach ($folder->getFiles() as $file) {
            $this->addFileToZip($file, $filePrefix);
        }

        foreach ($folder->getChildrens() as $children) {
            $this->addFolderContent($children, $filePrefix);
        }
    }

    public function addFileToZip(File $file, string $filePrefix = ''): void
    {
        if (!$this->authorizationChecker->isGranted(FileVoter::VIEW, $file)) {
            throw new AccessDeniedException();
        }

        $this->zip->addFile($filePrefix . $file->getName() . '.' . strtolower($file->getType()), $file->getContent());
    }
}
