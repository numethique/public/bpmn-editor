<?php

namespace App\Manager;

use App\Entity\Invitation;
use App\Entity\Project;
use App\Facade\ProjectInvitationFacade;
use Symfony\Component\Uid\UuidV1;

class InvitationManager
{
    public function handle(ProjectInvitationFacade $projectInvitationFacade, Project $project)
    {
        $invitations = [];

        foreach ($projectInvitationFacade->emails as $email) {
            $invitation = new Invitation();

            $invitation->setEmail($email);
            $invitation->setProject($project);
            $invitation->setText($projectInvitationFacade->text);
            $invitation->setProjectAccess($projectInvitationFacade->projectAccess);
            $invitation->setToken(UuidV1::generate());

            $invitations[] = $invitation;
        }

        return $invitations;
    }
}