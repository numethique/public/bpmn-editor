<?php

namespace App\Tests\Calculator;

use App\Calculator\FilesAndFolderCalculator;
use App\Entity\File;
use App\Entity\Folder;
use PHPUnit\Framework\TestCase;

class FilesAndFolderCalculatorTest extends TestCase
{
    protected $calculator;

    public function setUp(): void
    {
        $this->calculator = new FilesAndFolderCalculator();
    }

    public function testSimpleFolder()
    {
        $file = new File();
        $folder = new Folder();
        $folder->addFile($file);

        $this->assertSame([1, 1], $this->calculator->calculate($folder));
    }

    public function testSimpleFolderWithMultipleFiles()
    {
        $file = new File();
        $folder = new Folder();
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addFile($file);

        $this->assertSame([1, 3], $this->calculator->calculate($folder));
    }

    public function testChildrenFolderWithMultipleFiles()
    {
        $file = new File();
        $folder = new Folder();
        $children = new Folder();
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addChildren($children);

        $this->assertSame([2, 3], $this->calculator->calculate($folder));
    }

    public function testGrandChildrenFolderWithMultipleFiles()
    {
        $file = new File();
        $folder = new Folder();
        $children = new Folder();
        $grandChildren = new Folder();
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addChildren($children);
        $children->addChildren($grandChildren);

        $this->assertSame([3, 3], $this->calculator->calculate($folder));
    }

    public function testGrandGrandChildrenFolderWithMultipleFiles()
    {
        $file = new File();
        $folder = new Folder();
        $children = new Folder();
        $grandChildren = new Folder();
        $grandGrandChildren = new Folder();
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addChildren($children);
        $children->addChildren($grandChildren);
        $grandChildren->addChildren($grandGrandChildren);

        $this->assertSame([4, 3], $this->calculator->calculate($folder));
    }

    public function testGrandGrandChildrenFolderWithMultipleFilesInChildrens()
    {
        $file = new File();
        $folder = new Folder();
        $children = new Folder();
        $grandChildren = new Folder();
        $grandGrandChildren = new Folder();
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addFile($file);
        $folder->addChildren($children);
        $children->addFile($file);
        $children->addFile($file);
        $children->addFile($file);
        $children->addChildren($grandChildren);
        $grandChildren->addFile($file);
        $grandChildren->addChildren($grandGrandChildren);
        $grandGrandChildren->addFile($file);

        $this->assertSame([4, 8], $this->calculator->calculate($folder));
    }
}
