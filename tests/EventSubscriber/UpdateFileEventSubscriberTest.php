<?php

namespace App\Tests\EventSubscriber;

use App\Entity\File;
use App\Entity\User;
use App\Event\FileEvent;
use App\EventSubscriber\UpdateFileEventSubscriber;
use PHPUnit\Framework\TestCase;
use Phake;
use Pusher\Pusher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UpdateFileEventSubscriberTest extends TestCase
{
    protected $pusher;
    protected $subscriber;

    protected function setUp(): void
    {
        $this->pusher = Phake::mock(Pusher::class);

        $this->subscriber = new UpdateFileEventSubscriber($this->pusher);
    }

    public function testIsEventSubscriber()
    {
        $this->assertInstanceOf(EventSubscriberInterface::class, $this->subscriber);
    }

    public function testSubscribedEvent()
    {
        $this->assertSame([
            FileEvent::UPDATE => 'onFileUpdate',
            FileEvent::ADD_ATTENTION => 'onAddAttention',
            FileEvent::DELETE_ATTENTION => 'onDeleteAttention',
            KernelEvents::TERMINATE => 'sendEvents',
        ], UpdateFileEventSubscriber::getSubscribedEvents());
    }

    public function testFileUpdateEvent()
    {
        $dateTime = Phake::mock(\DateTimeImmutable::class);
        Phake::when($dateTime)->getTimestamp()->thenReturn('timestamp');

        $file = Phake::mock(File::class);
        Phake::when($file)->getUuid()->thenReturn('fileUuid');
        Phake::when($file)->getUpdatedAt()->thenReturn($dateTime);
        Phake::when($file)->getRevision()->thenReturn(1);
        Phake::when($file)->getOriginAppInstanceId()->thenReturn('originAppInstance');

        $user = Phake::mock(User::class);
        Phake::when($user)->getUuid('userId');
        Phake::when($user)->getName('userName');

        $fileEvent = Phake::mock(FileEvent::class);
        Phake::when($fileEvent)->getFile()->thenReturn($file);
        Phake::when($fileEvent)->getUser()->thenReturn($user);
        Phake::when($fileEvent)->getFileId()->thenReturn('fileId');

        Phake::when($this->pusher)->trigger(Phake::anyParameters())->thenReturn(json_decode('{}'));

        $this->subscriber->onFileUpdate($fileEvent);

        Phake::verify($this->pusher, Phake::never())->trigger(Phake::anyParameters());

        $kerneEvent = Phake::mock(KernelEvent::class);
        $this->subscriber->sendEvents($kerneEvent);

        Phake::verify($this->pusher)->trigger('private-diagram-update-fileId', 'diagram:update', [
            'type' => 'DIAGRAM_LAST_MODIFIED',
            'payload' => '{"fileId":"fileUuid","timestamp":"timestamp","user":{"id":"","name":"","username":""},"revision":1,"originAppInstanceId":"originAppInstance"}'
        ]);
    }
}
