<?php

namespace App\Tests\Security;

use App\Entity\File;
use App\Entity\Folder;
use App\Entity\Project;
use App\Entity\ProjectUser;
use App\Entity\User;
use App\Repository\ProjectUserRepository;
use App\Security\FileVoter;
use Phake;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FileVoterTest extends TestCase
{
    /**
     * @var FileVoter
     */
    protected $voter;

    protected $tokenStorage;
    protected $projectUserRepository;

    protected function setUp(): void
    {
        $this->tokenStorage = Phake::mock(TokenInterface::class);
        $this->projectUserRepository = Phake::mock(ProjectUserRepository::class);

        $this->voter = new FileVoter($this->projectUserRepository);
    }

    public function testInstanceType()
    {
        $this->assertInstanceOf(Voter::class, $this->voter);
    }

    /**
     * @param mixed $subject
     *
     * @dataProvider provideMultipleSubject
     */
    public function testDoNotVoteOnNonFile($subject)
    {
        $this->assertSame(Voter::ACCESS_ABSTAIN, $this->voter->vote($this->tokenStorage, $subject, [FileVoter::EDIT]));
    }

    public static function provideMultipleSubject()
    {
        return [
            [''],
            [1],
            [new \stdClass()],
            [new Folder()],
        ];
    }

    /**
     * @param mixed $attributes
     *
     * @dataProvider provideMultipleAttributes
     */
    public function testDoNotVoteOnNonAttribute($attributes)
    {
        $this->assertSame(Voter::ACCESS_ABSTAIN, $this->voter->vote($this->tokenStorage, new File(), $attributes));
    }

    public static function provideMultipleAttributes()
    {
        return [
            [['test']],
            [['foo']],
            [['bar']],
        ];
    }

    /**
     * @param string $expectedAccess
     * @param string $attribute
     * @param string $permission
     *
     * @dataProvider provideAttributeAndPermission
     */
    public function testVoteAccessOnFileFromProject($expectedAccess, $attribute, $permission)
    {
        $subject = Phake::mock(File::class);

        $user = Phake::mock(User::class);
        Phake::when($this->tokenStorage)->getUser()->thenReturn($user);

        $project = Phake::mock(Project::class);
        Phake::when($subject)->getProject()->thenReturn($project);

        $projectUser = Phake::mock(ProjectUser::class);
        Phake::when($this->projectUserRepository)->findOneByUserIdAndInternalProjectId(Phake::anyParameters())->thenReturn($projectUser);
        Phake::when($projectUser)->getPermission()->thenReturn($permission);

        $this->assertSame($expectedAccess, $this->voter->vote($this->tokenStorage, $subject, [$attribute]));
    }

    public static function provideAttributeAndPermission()
    {
        return [
            [Voter::ACCESS_GRANTED, FileVoter::EDIT, ProjectUser::OWNER],
            [Voter::ACCESS_GRANTED, FileVoter::COMMENT, ProjectUser::OWNER],
            [Voter::ACCESS_GRANTED, FileVoter::VIEW, ProjectUser::OWNER],
            [Voter::ACCESS_GRANTED, FileVoter::EDIT, ProjectUser::EDITOR],
            [Voter::ACCESS_GRANTED, FileVoter::COMMENT, ProjectUser::EDITOR],
            [Voter::ACCESS_GRANTED, FileVoter::VIEW, ProjectUser::EDITOR],
            [Voter::ACCESS_DENIED, FileVoter::EDIT, ProjectUser::COMMENT],
            [Voter::ACCESS_GRANTED, FileVoter::COMMENT, ProjectUser::COMMENT],
            [Voter::ACCESS_GRANTED, FileVoter::VIEW, ProjectUser::COMMENT],
            [Voter::ACCESS_DENIED, FileVoter::EDIT, ProjectUser::READ],
            [Voter::ACCESS_DENIED, FileVoter::COMMENT, ProjectUser::READ],
            [Voter::ACCESS_GRANTED, FileVoter::VIEW, ProjectUser::READ],
        ];
    }

    public function testVoteAccessDeniedOnFileFromOtherProject()
    {
        $subject = Phake::mock(File::class);

        $user = Phake::mock(User::class);
        Phake::when($this->tokenStorage)->getUser()->thenReturn($user);

        $project = Phake::mock(Project::class);
        Phake::when($subject)->getProject()->thenReturn($project);

        Phake::when($this->projectUserRepository)->findOneByUserIdAndInternalProjectId(Phake::anyParameters())->thenReturn(null);

        $this->assertSame(Voter::ACCESS_DENIED, $this->voter->vote($this->tokenStorage, $subject, [FileVoter::VIEW]));
    }
}
