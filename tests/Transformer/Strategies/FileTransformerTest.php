<?php

namespace App\Tests\Transformer\Strategies;

use App\Entity\File;
use App\Facade\FileFacade;
use App\Facade\UserFacade;
use App\Repository\FolderRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProjectRepository;
use App\Transformer\Strategies\FileTransformer;
use App\Transformer\Strategies\UserTransformer;
use App\Transformer\TransformerInterface;
use Phake;
use PHPUnit\Framework\TestCase;

class FileTransformerTest extends TestCase
{
    protected $transformer;

    protected $authorTransformer;
    protected $folderRepository;
    protected $projectRepository;
    protected $organizationRepository;

    protected function setUp(): void
    {
        $this->authorTransformer = Phake::mock(UserTransformer::class);
        $this->folderRepository = Phake::mock(FolderRepository::class);
        $this->projectRepository = Phake::mock(ProjectRepository::class);
        $this->organizationRepository = Phake::mock(OrganizationRepository::class);

        $this->transformer = new FileTransformer(
            $this->authorTransformer,
            $this->folderRepository,
            $this->projectRepository,
            $this->organizationRepository
        );
    }

    public function testCaseShare()
    {
        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('name');
        Phake::when($file)->getType()->thenReturn('BPMN');
        Phake::when($file)->getUuid()->thenReturn('uuid');

        $facade = $this->transformer->transform($file, TransformerInterface::SHARE);

        $this->assertInstanceOf(FileFacade::class, $facade);
        $this->assertSame('name', $facade->name);
        $this->assertSame('BPMN', $facade->type);
        $this->assertSame('uuid--name', $facade->slug);
        $this->assertSame('uuid--name', $facade->fileSlug);
        $this->assertNull($facade->id);
        $this->assertNull($facade->content);
    }

    public function testCaseDisplayShare()
    {
        $author = Phake::mock(UserFacade::class);
        Phake::when($this->authorTransformer)->transform(Phake::anyParameters())->thenReturn($author);

        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('name');
        Phake::when($file)->getType()->thenReturn('BPMN');
        Phake::when($file)->getUuid()->thenReturn('uuid');
        Phake::when($file)->getContent()->thenReturn('content');

        $facade = $this->transformer->transform($file, TransformerInterface::DISPLAY_SHARE);

        $this->assertInstanceOf(FileFacade::class, $facade);
        $this->assertSame('name', $facade->name);
        $this->assertSame('BPMN', $facade->type);
        $this->assertNull($facade->slug);
        $this->assertNull($facade->fileSlug);
        $this->assertNull($facade->id);
        $this->assertSame('content', $facade->content);
    }

    public function testCaseOnlyFile()
    {
        $author = Phake::mock(UserFacade::class);
        Phake::when($this->authorTransformer)->transform(Phake::anyParameters())->thenReturn($author);

        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('name');
        Phake::when($file)->getType()->thenReturn('BPMN');
        Phake::when($file)->getUuid()->thenReturn('uuid');
        Phake::when($file)->getContent()->thenReturn('content');

        $facade = $this->transformer->transform($file, TransformerInterface::ONLY_FILE);

        $this->assertInstanceOf(FileFacade::class, $facade);
        $this->assertSame('name', $facade->name);
        $this->assertSame('BPMN', $facade->type);
        $this->assertSame('uuid--name', $facade->slug);
        $this->assertSame('uuid--name', $facade->fileSlug);
        $this->assertSame('uuid', $facade->id);
        $this->assertSame('content', $facade->content);
    }

    public function testNoCase()
    {
        $author = Phake::mock(UserFacade::class);
        Phake::when($this->authorTransformer)->transform(Phake::anyParameters())->thenReturn($author);

        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('name');
        Phake::when($file)->getType()->thenReturn('BPMN');
        Phake::when($file)->getUuid()->thenReturn('uuid');
        Phake::when($file)->getContent()->thenReturn('content');

        $facade = $this->transformer->transform($file);

        $this->assertInstanceOf(FileFacade::class, $facade);
        $this->assertSame('name', $facade->name);
        $this->assertSame('BPMN', $facade->type);
        $this->assertSame('uuid--name', $facade->slug);
        $this->assertSame('uuid--name', $facade->fileSlug);
        $this->assertSame('uuid', $facade->id);
        $this->assertNull($facade->content);
    }
}
