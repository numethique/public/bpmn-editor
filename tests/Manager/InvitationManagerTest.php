<?php

namespace App\Tests\Manager;

use App\Entity\Invitation;
use App\Entity\Project;
use App\Facade\ProjectInvitationFacade;
use App\Manager\InvitationManager;
use PHPUnit\Framework\TestCase;
use Phake;

class InvitationManagerTest extends TestCase
{
    protected $manager;

    protected function setUp():void
    {
        $this->manager = new InvitationManager();
    }

    public function testNoInvitation()
    {
        $project = Phake::mock(Project::class);
        $projectInvitation = new ProjectInvitationFacade();

        $accessRight = 'WRITE';
        $projectInvitation->projectAccess = $accessRight;
        $mailText = 'foo bar';
        $projectInvitation->text = $mailText;

        $output = $this->manager->handle($projectInvitation, $project);

        $this->assertCount(0, $output);
    }

    public function testSimpleInvitation()
    {
        $project = Phake::mock(Project::class);
        $projectInvitation = new ProjectInvitationFacade();

        $invitationEmail = 'test@test.com';
        $projectInvitation->emails = [$invitationEmail];
        $accessRight = 'WRITE';
        $projectInvitation->projectAccess = $accessRight;
        $mailText = 'foo bar';
        $projectInvitation->text = $mailText;

        $output = $this->manager->handle($projectInvitation, $project);

        $this->assertCount(1, $output);
        $invitation = array_pop($output);
        $this->assertInstanceOf(Invitation::class, $invitation);
        $this->assertSame($invitationEmail, $invitation->getEmail());
        $this->assertNotNull($invitation->getToken());
        $this->assertSame($mailText, $invitation->getText());
        $this->assertSame($accessRight, $invitation->getProjectAccess());
        $this->assertSame($project, $invitation->getProject());
    }

    public function testMultipleInvitation()
    {
        $project = Phake::mock(Project::class);
        $projectInvitation = new ProjectInvitationFacade();

        $invitationEmail = 'test@test.com';
        $projectInvitation->emails = [$invitationEmail, $invitationEmail];
        $accessRight = 'WRITE';
        $projectInvitation->projectAccess = $accessRight;
        $mailText = 'foo bar';
        $projectInvitation->text = $mailText;

        $output = $this->manager->handle($projectInvitation, $project);

        $this->assertCount(2, $output);
        $invitation = array_pop($output);
        $this->assertInstanceOf(Invitation::class, $invitation);
        $this->assertSame($invitationEmail, $invitation->getEmail());
        $this->assertNotNull($invitation->getToken());
        $this->assertSame($mailText, $invitation->getText());
        $this->assertSame($accessRight, $invitation->getProjectAccess());
        $this->assertSame($project, $invitation->getProject());
    }
}
