<?php

namespace App\Tests\Manager;

use App\Entity\File;
use App\Entity\Folder;
use App\Facade\DownloadFileFacade;
use App\Manager\DownloadManager;
use App\Repository\FileRepository;
use App\Repository\FolderRepository;
use App\Security\FileVoter;
use Phake;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use ZipStream\ZipStream;

class DownloadManagerTest extends TestCase
{
    protected $manager;

    protected $fileRepository;
    protected $folderRepository;
    protected $authorizationChecker;

    protected function setUp(): void
    {
        $this->fileRepository = Phake::mock(FileRepository::class);
        $this->folderRepository = Phake::mock(FolderRepository::class);
        $this->authorizationChecker = Phake::mock(AuthorizationCheckerInterface::class);

        $this->manager = new DownloadManager($this->fileRepository, $this->folderRepository, $this->authorizationChecker);
    }

    public function testDownloadAllFolder()
    {
        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('foo');
        Phake::when($file)->getContent()->thenReturn('bar');
        Phake::when($file)->getType()->thenReturn(File::TYPE_BPMN);

        Phake::when($this->authorizationChecker)->isGranted(Phake::anyParameters())->thenReturn(true);

        $folder = Phake::mock(Folder::class);
        Phake::when($folder)->getName()->thenReturn('folder');
        Phake::when($folder)->getFiles()->thenReturn([$file, $file]);
        Phake::when($folder)->getChildrens()->thenReturn([]);

        Phake::when($this->fileRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($file);
        Phake::when($this->folderRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($folder);

        $zip = Phake::mock(ZipStream::class);

        $facade = new DownloadFileFacade();
        $facade->fileIds[] = 'foo';
        $facade->fileIds[] = 'foo';
        $facade->folderIds[] = 'foo';
        $facade->folderIds[] = 'foo';

        $this->manager->execute($facade, $zip);

        Phake::verify($zip, Phake::times(2))->addFile('foo.bpmn', 'bar');
        Phake::verify($zip, Phake::times(4))->addFile('folder/foo.bpmn', 'bar');
        Phake::verify($this->authorizationChecker, Phake::times(6))->isGranted(Phake::anyParameters());
    }

    public function testDownloadAllFolderAndSubFolder()
    {
        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('foo');
        Phake::when($file)->getContent()->thenReturn('bar');
        Phake::when($file)->getType()->thenReturn(File::TYPE_BPMN);

        Phake::when($this->authorizationChecker)->isGranted(Phake::anyParameters())->thenReturn(true);

        $grandChild = Phake::mock(Folder::class);
        Phake::when($grandChild)->getName()->thenReturn('grandChildFolder');
        Phake::when($grandChild)->getFiles()->thenReturn([$file, $file]);
        Phake::when($grandChild)->getChildrens()->thenReturn([]);

        $childFolder = Phake::mock(Folder::class);
        Phake::when($childFolder)->getName()->thenReturn('childFolder');
        Phake::when($childFolder)->getFiles()->thenReturn([$file, $file]);
        Phake::when($childFolder)->getChildrens()->thenReturn([$grandChild, $grandChild]);

        $folder = Phake::mock(Folder::class);
        Phake::when($folder)->getName()->thenReturn('folder');
        Phake::when($folder)->getFiles()->thenReturn([$file, $file]);
        Phake::when($folder)->getChildrens()->thenReturn([$childFolder, $childFolder]);

        Phake::when($this->fileRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($file);
        Phake::when($this->folderRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($folder);

        $zip = Phake::mock(ZipStream::class);

        $facade = new DownloadFileFacade();
        $facade->fileIds[] = 'foo';
        $facade->fileIds[] = 'foo';
        $facade->folderIds[] = 'foo';
        $facade->folderIds[] = 'foo';

        $this->manager->execute($facade, $zip);

        Phake::verify($zip, Phake::times(2))->addFile('foo.bpmn', 'bar');
        Phake::verify($zip, Phake::times(4))->addFile('folder/foo.bpmn', 'bar');
        Phake::verify($zip, Phake::times(8))->addFile('folder/childFolder/foo.bpmn', 'bar');
        Phake::verify($zip, Phake::times(16))->addFile('folder/childFolder/grandChildFolder/foo.bpmn', 'bar');
        Phake::verify($this->authorizationChecker, Phake::times(30))->isGranted(Phake::anyParameters());
    }

    public function testDownloadFailedForInsuficiantRight()
    {
        $file = Phake::mock(File::class);
        Phake::when($file)->getName()->thenReturn('foo');
        Phake::when($file)->getContent()->thenReturn('bar');
        Phake::when($file)->getType()->thenReturn(File::TYPE_BPMN);

        Phake::when($this->authorizationChecker)->isGranted(FileVoter::VIEW, $file)->thenReturn(false);

        $folder = Phake::mock(Folder::class);
        Phake::when($folder)->getName()->thenReturn('folder');
        Phake::when($folder)->getFiles()->thenReturn([$file, $file]);
        Phake::when($folder)->getChildrens()->thenReturn([]);

        Phake::when($this->fileRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($file);
        Phake::when($this->folderRepository)->findOneByUuid(Phake::anyParameters())->thenReturn($folder);

        $zip = Phake::mock(ZipStream::class);

        $facade = new DownloadFileFacade();
        $facade->fileIds[] = 'foo';
        $facade->fileIds[] = 'foo';
        $facade->folderIds[] = 'foo';
        $facade->folderIds[] = 'foo';

        $this->expectException(AccessDeniedException::class);

        $this->manager->execute($facade, $zip);
    }
}
