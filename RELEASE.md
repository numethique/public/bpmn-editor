# Release notes

## 0.8.0
* Add forgot password feature

## 0.7.3
* Add search index on file

## 0.7.2
* Upgrade to symfony 7.1
* Send pusher event after kernel terminate on file update

## 0.7.1
* Increase query performance

## 0.7.0
* Add file right check

## 0.6.2
* Fix right author on file duplicate
* Remove terms on register

## 0.6.1
* Fix missing migration

## 0.6.0
* Add project level access management
* Increase session lifetime
* Update logo

## 0.5.0
* Add share by email feature

## 0.4.1
* Fix by using millisecond format for timestamp `format('Uv')`

## 0.4.0
* Fix updated at display date
* Add revision increment

## 0.3.0

* Profile page
* Leave organization
* Text in invitation email
* Add file type in archive download

## 0.2.0

* File download
* Fix delete invitation
